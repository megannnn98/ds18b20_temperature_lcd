
#pragma once

//** PWR_DOWN ******************************************************************************************************

#define POWER_GPIO				GPIOE
#define POWER_PIN				GPIO_Pin_6
			  
#define POWER_ON  				POWER_GPIO->ODR |=  POWER_PIN
#define POWER_OFF 				POWER_GPIO->ODR &= ~POWER_PIN
                                                     
#define mcuPowerOff()  			PIN_POWER_OFF


//** BUTTON 

#define BTN_GPIO		 		GPIOE
#define BTN_PIN                 GPIO_Pin_7
#define BTN_EXTI_PIN                 EXTI_Pin_7

#define IS_BUTTON_PRESSED       (BTN_PIN  == (BTN_GPIO->IDR & BTN_PIN ))
#define IS_BUTTON_UNPRESSED     (!IS_BUTTON_PRESSED)


//** SPI 

#define SPI_MISO_PIN                  GPIO_Pin_2 // PA2
#define SPI_MOSI_PIN                  GPIO_Pin_3 // PA3
#define SPI_SCK_PIN                   GPIO_Pin_6 // PC6
#define SPI_CS_PIN                    GPIO_Pin_5 // PC5

#define SPI_MISO_GPIO		          GPIOA
#define SPI_MOSI_GPIO		          GPIOA
#define SPI_SCK_GPIO	              GPIOC
#define SPI_CS_GPIO              	  GPIOC
                                                     
#define WAIT_UNTIL_MISO_BECOMES_LOW   while(SPI_MISO_PIN == (SPI_MISO_GPIO->IDR & SPI_MISO_PIN))


#define CS_HIGH                       do {SPI_CS_GPIO->ODR |=  SPI_CS_PIN; } while(0)
#define CS_LOW                        do {SPI_CS_GPIO->ODR &= ~SPI_CS_PIN; } while(0)

#define RCHPP_SPI					  SPI1


// ** GDO0
// ** GDO2
			  
#define GDO0_GPIO		 			GPIOC
#define GDO2_GPIO		 			GPIOC

#define GDO0_PIN         			GPIO_Pin_0
#define GDO2_PIN         			GPIO_Pin_1

#define GDO0_ISR_EN     			GDO2_GPIO->CR2 |=  GDO0_PIN
#define GDO0_ISR_DIS    			GDO2_GPIO->CR2 &= ~GDO0_PIN

#define IS_GDO0_ACTIVE	 (GDO0_PIN  == (GDO0_GPIO->IDR & GDO0_PIN))
#define IS_GDO0_INACTIVE (GDO0_PIN  != (GDO0_GPIO->IDR & GDO0_PIN))

#define EXTI_GDO0_Falling       EXTI_SetPinSensitivity(EXTI_Pin_0, EXTI_Trigger_Falling)

// GDO2 == 0 �������� ���������
// GDO2 == 0 ���������� ���������
#define IS_PREAMBLE_DETECTED 	(GDO2_PIN  != (GDO2_GPIO->IDR & GDO2_PIN))


// ** ADC

#define ADC_GPIO				 GPIOF
#define ADC_PIN					(0x01)

			  


// ** 


#ifdef DEBUG
	#define TIM_FOR_DISABLE     (u16)6000
	#define TIM_FOR_SHOW		(u16)200
#else
	#define TIM_FOR_DISABLE     (u16)60000
	#define TIM_FOR_SHOW		(u16)2000
#endif

#define TIM_FOR_BLINKING    (u16)10000
