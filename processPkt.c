/**
  ******************************************************************************
  * @file    processPkt.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Process of UART calibration 
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "calibration.h"
#include "ade7953.h"
#include "calcCRC.h"
#include "eeCoding.h"

/* Private define ------------------------------------------------------------*/
#define PKT_CAL_CMD			1			
#define PKT_CAL_DATA_LEN	0


/* Global variables ---------------------------------------------------------*/

extern struct
{
	uint32_t AWGain;           /* Active energy gain (Current Channel A) */
	uint32_t AVarGain;         /* Reactive energy gain (Current Channel A) */
	uint32_t AVAGain;          /* Apparent energy gain (Current Channel A) */
	uint32_t AWattOs;          /* Active energy offset correction (Current Channel A) */
	uint32_t AVarOs;           /* Reactive energy offset correction (Current Channel A) */
	uint32_t AVAOs;            /* Apparent energy offset correction (Current Channel A) */
	uint32_t AIrmsOs;          /* IRMS offset (Current Channel A) */
	uint32_t VrmsOs;           /* VRMS offset */
	uint32_t PhCalA;           /* Phase calibration register (Current Channel A). This register is in sign magnitude format.
	10 bits: bits 0..8 - value 0,021 degree/LSB; bit 9 - sign */

	uint32_t AIGAIN;          /* AIGAIN IRMS scale */
	uint32_t VGAIN;            /* VGAIN  VRMS scale*/  

	uint32_t DateTime;         /* ���� ���������� � ��������*/
} CalAde7953;


extern struct {
	float Frequency; 
	float V; 
	float I;

	float APower;
	float RPower;
	float APPower;
//	float PowerFactor;
	
} rms;


extern __no_init struct 
{
	u32 val;
	u16 crc;
} timeOfWork;
extern __no_init struct 
{
	u32 val;
	u16 crc;
} telemetry;


extern struct{
	u32 number;
	u32 version;
	u32 build;
	u32 type;
} factory;
extern struct {
  
	u8 buf_RX[10];
	u8 buf_TX[TXBUF_SIZE];
  	u8 counterRX;
	u8 counterTX;
  	u8 bytesForSend;
} usart;


/* Private function prototypes -----------------------------------------------*/

MEM_STATUS_t CalAde7953_Save(void);
void processPkt();
void getParameters();
void setParameters();
float Convert_IEEE754_to_Microchip(float Input);


/* Private functions ---------------------------------------------------------*/


/**
  * @brief  ���������� ����������� ����� � ����� ������
  * @note ������ ������� - ����� - ����������� - [(�����������)������] - CRC
  * @retval None
  */
void RS_485_AddCRC_Cal()
{
	u8 cnt = 0;
	
	cnt = usart.buf_TX[0] - 2;
	
	usart.buf_TX[cnt++]  = BYTE_1(CRC16(usart.buf_TX,usart.buf_TX[0]-2));
	usart.buf_TX[cnt]    = BYTE_0(CRC16(usart.buf_TX,usart.buf_TX[0]-2));
	
}

/**
  * @brief  ������������ ��������� ��� ������, ��� ��������� ������
  * @param  ��� ������
  * @retval None
  */
void RS_485_SendERR(u8 ErrCode)
{
	usart.buf_TX[0] = 5;
	usart.buf_TX[1] = usart.buf_RX[PKT_CAL_CMD] | 0x80;
	usart.buf_TX[2] = ErrCode;
	
	RS_485_AddCRC_Cal();
}


/**
  * @brief  ������������ ��������� ��� ��� ��� ������
  * @param  ��� ������
  * @retval None
  */
static void RS_485_SetPktHead(uint8_t length)
{
	usart.buf_TX[0] = length;
	usart.buf_TX[1] = usart.buf_RX[PKT_CAL_CMD];
}


#define ValBit_00(VAR,Place)         (((u8)(VAR) & (u8)((u8)1<<(u8)(Place))) >> (u8)(Place))


/**
  * @brief  ������������ ������ �� �������� �� UART �����
  * @retval None
  */
void processPkt()
{
  	static union {
	  	
	  	u32   val32;
		float valF;
	} microchip;
	  
	switch(usart.buf_RX[PKT_CAL_CMD])
	{
		case SERIAL_CMD_TYPE_0: //04 00 03 70 
		{
			if(4 != usart.buf_RX[PKT_CAL_DATA_LEN] ) 
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(10);
				usart.buf_TX[2] = BYTE_0(factory.build);
				usart.buf_TX[3] = BYTE_0(factory.version);
				usart.buf_TX[4] = BYTE_0(factory.type);
				usart.buf_TX[5] = BYTE_1(factory.type);
				usart.buf_TX[6] = BYTE_2(factory.type);
			}
		}
		break;
		case SERIAL_CMD_NUMBER_0: // 04 04 02 B3
		{
			if(4 != usart.buf_RX[PKT_CAL_DATA_LEN])
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(7);
				usart.buf_TX[2] = BYTE_0(factory.number);
				usart.buf_TX[3] = BYTE_1(factory.number);
				usart.buf_TX[4] = BYTE_2(factory.number);
			}
		}
		break;
		case SERIAL_CMD_NUMBER_1: // 07 05 xx xx xx CRC
		{
			if(7 != usart.buf_RX[PKT_CAL_DATA_LEN])
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(4);
				factory.number = CREATE_LONG_VAL(usart.buf_RX[4],usart.buf_RX[3],usart.buf_RX[2]);
				if (STATUS_ERROR == wrapperWrite(I_EE_NUMDDM, &factory.number))	
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case SERIAL_CMD_CALIBR_0:
		{
			if(5 != usart.buf_RX[PKT_CAL_DATA_LEN])
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				getParameters();
			} // endif
			
		}
		break;
		// ** ������ ������������� ��������
		case SERIAL_CMD_CALIBR_1:
		{
			setParameters();
		}
		break;
		
		case SERIAL_CMD_ADE_RESET: // 04 10 02 BC 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(4);
          msg.set(ADE_RESET_MSG, NULL, 0);
			}
		}
		break;
		case SERIAL_CMD_ADE_DEFAULT: // 04 11 C3 7C 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(4);
				CalAde7953.AWGain     = AWGAIN_DEFAULT;
				CalAde7953.AVarGain   = AVARGAIN_DEFAULT;
				CalAde7953.AVAGain    = AVAGAIN_DEFAULT;
				CalAde7953.AWattOs    = AWATTOS_DEFAULT;
				CalAde7953.AVarOs     = AVAROS_DEFAULT;
				CalAde7953.AVAOs      = AVAOS_DEFAULT;
				CalAde7953.AIrmsOs    = AIRMSOS_DEFAULT;
				CalAde7953.VrmsOs     = VRMSOS_DEFAULT;
				CalAde7953.PhCalA     = PHCALA_DEFAULT;
				CalAde7953.AIGAIN 	  = AIGAIN_DEFAULT;
				CalAde7953.VGAIN  	  = VGAIN_DEFAULT;
				
				if (STATUS_ERROR == CalAde7953_Save())
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
			
		}
		break;
		case SERIAL_CMD_APOWER: // 04 12 83 7D 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				microchip.valF = Convert_IEEE754_to_Microchip(rms.APower);
				usart.buf_TX[2] = BYTE_0(microchip.val32);
				usart.buf_TX[3] = BYTE_1(microchip.val32);
				usart.buf_TX[4] = BYTE_2(microchip.val32);
				usart.buf_TX[5] = BYTE_3(microchip.val32);
			}

		}
		break;
		
		case SERIAL_CMD_RPOWER: // 04 13 42 BD 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				microchip.valF = Convert_IEEE754_to_Microchip(rms.RPower);
				usart.buf_TX[2] = BYTE_0(microchip.val32);
				usart.buf_TX[3] = BYTE_1(microchip.val32);
				usart.buf_TX[4] = BYTE_2(microchip.val32);
				usart.buf_TX[5] = BYTE_3(microchip.val32);
			}
		}
		break;
		case SERIAL_CMD_FPOWER: // 04 14 03 7F 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				microchip.valF = Convert_IEEE754_to_Microchip(rms.APPower);
				usart.buf_TX[2] = BYTE_0(microchip.val32);
				usart.buf_TX[3] = BYTE_1(microchip.val32);
				usart.buf_TX[4] = BYTE_2(microchip.val32);
				usart.buf_TX[5] = BYTE_3(microchip.val32);
			}
		}
		break;
		case SERIAL_CMD_IRMS: // 04 15 C2 BF 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				microchip.valF = Convert_IEEE754_to_Microchip(rms.I);
				usart.buf_TX[2] = BYTE_0(microchip.val32);
				usart.buf_TX[3] = BYTE_1(microchip.val32);
				usart.buf_TX[4] = BYTE_2(microchip.val32);
				usart.buf_TX[5] = BYTE_3(microchip.val32);
			}
		}
		break;
		case SERIAL_CMD_VRMS: // 04 16 82 BE 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				microchip.valF = Convert_IEEE754_to_Microchip(rms.V);
				usart.buf_TX[2] = BYTE_0(microchip.val32);
				usart.buf_TX[3] = BYTE_1(microchip.val32);
				usart.buf_TX[4] = BYTE_2(microchip.val32);
				usart.buf_TX[5] = BYTE_3(microchip.val32);
			}
		}
		break;
		case SERIAL_CMD_FREQ: // 04 17 43 7E 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				microchip.valF = Convert_IEEE754_to_Microchip(rms.Frequency);
				usart.buf_TX[2] = BYTE_0(microchip.val32);
				usart.buf_TX[3] = BYTE_1(microchip.val32);
				usart.buf_TX[4] = BYTE_2(microchip.val32);
				usart.buf_TX[5] = BYTE_3(microchip.val32);
			}
		}
		break;
		
		case GET_TIME_OF_WORK: // 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				usart.buf_TX[2] = BYTE_0(timeOfWork.val);
				usart.buf_TX[3] = BYTE_1(timeOfWork.val);
				usart.buf_TX[4] = BYTE_2(timeOfWork.val);
				usart.buf_TX[5] = BYTE_3(timeOfWork.val);
			}
		}
		break;	
		
		  
		case GET_PULSES: // 
		{
			if(usart.buf_RX[0] != 4)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(8);
				usart.buf_TX[2] = BYTE_0(telemetry.val);
				usart.buf_TX[3] = BYTE_1(telemetry.val);
				usart.buf_TX[4] = BYTE_2(telemetry.val);
				usart.buf_TX[5] = BYTE_3(telemetry.val);
			}
		}
		break;	
		
		default:
		{
			RS_485_SendERR(SERIAL_ERR_FUNC);
		}
	} // endSwitch
}



// ** --------------------------------------------------------------------------
/**
  * @brief  ������������ ������ �� ������� ������, � ���������� �������
  * @retval None
  */
void getParameters()
{
	switch(usart.buf_RX[2]) // 05 08 xx CRC
	{
		case 0:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AWGain);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AWGain);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AWGain);										
		}
		break;
		case 1:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AVarGain);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AVarGain);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AVarGain);										
		}
		break;
		case 2:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AVAGain);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AVAGain);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AVAGain);										
		}
		break;
		case 3:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AWattOs);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AWattOs);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AWattOs);										
		}
		break;
		case 4:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AVarOs);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AVarOs);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AVarOs);										
		}
		break;
		case 5:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AVAOs);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AVAOs);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AVAOs);										
		}
		break;
		case 6:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AIrmsOs);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AIrmsOs);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AIrmsOs);										
		}
		break;
		case 7:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.VrmsOs);
			usart.buf_TX[4] = BYTE_1(CalAde7953.VrmsOs);
			usart.buf_TX[5] = BYTE_2(CalAde7953.VrmsOs);										
		}
		break;
		case 8:
		{
			RS_485_SetPktHead(7);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.PhCalA);
			usart.buf_TX[4] = BYTE_1(CalAde7953.PhCalA);										
		}
		break;

		case 0x0B:
		{
			RS_485_SetPktHead(7);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(ADE_CFxDEN);
			usart.buf_TX[4] = BYTE_1(ADE_CFxDEN);										
		}
		break;
		case 0x0E:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.AIGAIN);
			usart.buf_TX[4] = BYTE_1(CalAde7953.AIGAIN);
			usart.buf_TX[5] = BYTE_2(CalAde7953.AIGAIN);										
		}
		break;
		case 0x0F:
		{
			RS_485_SetPktHead(8);
			usart.buf_TX[2] = usart.buf_RX[2];
			usart.buf_TX[3] = BYTE_0(CalAde7953.VGAIN);
			usart.buf_TX[4] = BYTE_1(CalAde7953.VGAIN);
			usart.buf_TX[5] = BYTE_2(CalAde7953.VGAIN);										
		}
		break;
		default:
		{
			RS_485_SendERR(SERIAL_ERR_DAT);
		}
		
	} // endswitch
}








// ** --------------------------------------------------------------------------
/**
  * @brief  ������������ ������ �� ������� ������, � ���������� �������
  * @retval None
  */
void setParameters()
{
	switch(usart.buf_RX[2])
	{
		case 0:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				
				CalAde7953.AWGain = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);
				if (STATUS_ERROR == wrapperWrite(I_EE_AWGain, &CalAde7953.AWGain))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 1:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AVarGain =  CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);
				if (STATUS_ERROR == wrapperWrite(I_EE_AVarGain, &CalAde7953.AVarGain))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}	
			}
		}
		break;
		case 2:
		{
			if(usart.buf_RX[0] != 8)
			RS_485_SendERR(SERIAL_ERR_LEN);
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AVAGain = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);
				if (STATUS_ERROR == wrapperWrite(I_EE_AVAGain, &CalAde7953.AVAGain))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 3:
		{
			if(usart.buf_RX[0] != 8)
			RS_485_SendERR(SERIAL_ERR_LEN);
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AWattOs = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);

				if (STATUS_ERROR == wrapperWrite(I_EE_AWattOs, &CalAde7953.AWattOs))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 4:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AVarOs = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);
				if (STATUS_ERROR == wrapperWrite(I_EE_AVarOs, &CalAde7953.AVarOs))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 5:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AVAOs = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);						
				if (STATUS_ERROR == wrapperWrite(I_EE_AVAOs, &CalAde7953.AVAOs))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 6:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AIrmsOs = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);								
				if (STATUS_ERROR == wrapperWrite(I_EE_AIrmsOs, &CalAde7953.AIrmsOs))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 7:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.VrmsOs = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);										
				if (STATUS_ERROR == wrapperWrite(I_EE_VrmsOs, &CalAde7953.VrmsOs))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 8:
		{
			if(usart.buf_RX[0] != 7)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.PhCalA = CREATE_LONG_VAL(0,usart.buf_RX[4],usart.buf_RX[3]);										
				if (STATUS_ERROR == wrapperWrite(I_EE_PhCalA, &CalAde7953.PhCalA))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;

		case 0x0E:
		{

			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.AIGAIN = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);
				if (STATUS_ERROR == wrapperWrite(I_EE_AIGAIN, &CalAde7953.AIGAIN))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;
		case 0x0F:
		{
			if(usart.buf_RX[0] != 8)
			{
				RS_485_SendERR(SERIAL_ERR_LEN);
			}
			else
			{
				RS_485_SetPktHead(5);
				usart.buf_TX[2] = usart.buf_RX[2];
				CalAde7953.VGAIN = CREATE_LONG_VAL(usart.buf_RX[5],usart.buf_RX[4],usart.buf_RX[3]);
				if (STATUS_ERROR == wrapperWrite(I_EE_VGAIN, &CalAde7953.VGAIN))
				{
				  	RS_485_SendERR(SERIAL_ERR_MEM);
				}
			}
		}
		break;

		
		default:
		RS_485_SendERR(SERIAL_ERR_DAT);
	}// endswitch
}




/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/



