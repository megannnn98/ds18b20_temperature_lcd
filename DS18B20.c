#include "stm8l15x.h"
#include "DS18B20.h"
#include "utils.h"


// Delay in I * 5us
void _delay_x5_us(u16 i)
{
    i = i << 4;
    for(; i; i--);
}


bool ds_reset()
{
	static bool i = FALSE;
	
	OUTPUT_LOW();
		_delay_x5_us(480 _us);
		PORT_INPUT;
		_delay_x5_us(100 _us);
		i = (bool)ValBit(DS_port->IDR, DS_pin);   
		_delay_x5_us(480 _us);
	OUTPUT_HIGH();
	
	return i;
}



void ds_writeSlot(bool i)
{
  	OUTPUT_LOW();
        _delay_x5_us(10 _us);
        if(TRUE == i) 
        {
            PIN_HIGH;
        }
        _delay_x5_us(60 _us);
	PIN_HIGH;
}



u8 ds_readSlot (void)
{
  	static u8 i;
  
  	OUTPUT_LOW();
        _delay_x5_us(6 _us);
        PORT_INPUT;
        _delay_x5_us(9 _us);
        i = ValBit(DS_port->IDR, DS_pin);
        _delay_x5_us(55 _us);
	OUTPUT_HIGH();
	
	return i;
}


void ds_writeByte (u8 byte)
{
	static u8 i;
	for(i=0; i<8; i++)
	{
		if(byte & 0x01) 
        {
            ds_writeSlot(TRUE);
        }
		else 
        {
            ds_writeSlot(FALSE);
        }
		byte >>= 1;
	}
}



u8 ds_readByte (void)
{
	static u8 i, j, byte;
    i = 0, j = 0x1, byte = 0;
    
	for(i = 0; i < 8; i++)
	{
		if(ds_readSlot()) 
        {
            byte |= j;
        }
		j = j << 1;
	}	
	return byte;
}


float convertTemperToFloat(u8* p)
{
//  	static float itemperature = 0.0;
//        static s8    integer_part = 0;
//	
//        integer_part = ( p[1] << 4 )|( p[0] >> 4 );
//
//	itemperature  = (p[0] & 0x08)/2.;
//	itemperature += (float)integer_part;
//        
//        if (p[1] & 0xF0) {itemperature *= -1.;}
//
//	return itemperature;
    static u16  tmp16 = 0;
    static u8   integer_part = 0;
    static u8    double_part = 0;
    static float float_result = 0;
    
    
    tmp16 = (p[1] << 8) | p[0];
    double_part = p[0] & 0x0F;
    tmp16 = tmp16 & 0x7FF;
    tmp16 = tmp16 >> 3;
    integer_part = tmp16 >> 1;
        
    float_result = 0;
    if (double_part & 0x8) {
        float_result += 0.5;
    }
    if (double_part & 0x4) {
        float_result += 0.25;
    }
    if (double_part & 0x2) {
        float_result += 0.125;
    }
    if (double_part & 0x1) {
        float_result += 0.0625;
    }
    float_result += (float)integer_part;
    
    if (p[1] & 0xF0) {
        float_result *= -1;
    }
    return float_result;
}




float get_temperature()
{
    static u8 scratchpad[2] = {0};
  
    ds_reset();
    ds_writeByte(DS18B20_SKIP_ROM);
    ds_writeByte(DS18B20_CONVERT_T);
    
    while (0 == ds_readSlot());

    ds_reset();
    ds_writeByte(DS18B20_SKIP_ROM);
    ds_writeByte(DS18B20_READ_SCRATCHPAD);

    scratchpad[0] = ds_readByte();
    scratchpad[1] = ds_readByte();
    
    
    return convertTemperToFloat((u8*)scratchpad);
}


