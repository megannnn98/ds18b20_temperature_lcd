/**
  ******************************************************************************
  * @file    globals.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Module contain project common variables 
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "ade7953.h"


/* Global variables ---------------------------------------------------------*/


struct {
	float Frequency; 
	float V; 
	float I;

	float APower;
	float RPower;
	float APPower;
//	float PowerFactor;
	
} rms = {
  
	.Frequency = 0.,
	.V = 0.,
	.I = 0.,

	.APower = 0.,
	.RPower = 0.,
	.APPower = 0.,
//	.PowerFactor = 0.,
};


struct{
	u32 number;
	u32 version;
	u32 build;
	u32 type;
} factory = {
	.number = 0x0,	//
	.version = 0x1,	//
	.build = 0x1,		//
	.type = 0x11001,//
};

struct
{
	uint32_t AWGain;           /* Active energy gain (Current Channel A) */
	uint32_t AVarGain;         /* Reactive energy gain (Current Channel A) */
	uint32_t AVAGain;          /* Apparent energy gain (Current Channel A) */
	uint32_t AWattOs;          /* Active energy offset correction (Current Channel A) */
	uint32_t AVarOs;           /* Reactive energy offset correction (Current Channel A) */
	uint32_t AVAOs;            /* Apparent energy offset correction (Current Channel A) */
	uint32_t AIrmsOs;          /* IRMS offset (Current Channel A) */
	uint32_t VrmsOs;           /* VRMS offset */
	uint32_t PhCalA;           /* Phase calibration register (Current Channel A). This register is in sign magnitude format.
	10 bits: bits 0..8 - value 0,021 degree/LSB; bit 9 - sign */

	uint32_t AIGAIN;          /* AIGAIN IRMS scale */
	uint32_t VGAIN;            /* VGAIN  VRMS scale*/  

	uint32_t DateTime;         /* ���� ���������� � ��������*/
} CalAde7953 = {

	.AWGain   = AWGAIN_DEFAULT,
	.AVarGain = AVARGAIN_DEFAULT,
	.AVAGain  = AVAGAIN_DEFAULT,
	.AWattOs  = AWATTOS_DEFAULT,

	.AVarOs   = AVAROS_DEFAULT,
	.AVAOs    = AVAOS_DEFAULT,
	.AIrmsOs  = AIRMSOS_DEFAULT,
	.VrmsOs   = VRMSOS_DEFAULT,
	.PhCalA   = PHCALA_DEFAULT,
	  
	.AIGAIN   = AIGAIN_DEFAULT,
	.VGAIN    = VGAIN_DEFAULT,
	  
	.DateTime = 0
};


struct {
  
	u8 buf_RX[10];
	u8 buf_TX[TXBUF_SIZE];
  	u8 counterRX;
	u8 counterTX;
  	u8 bytesForSend;
} usart = {

	.buf_RX = {0},
	.buf_TX = {0},
  	.counterRX = 0,
	.counterTX = 0,
  	.bytesForSend = 0,
};

bool stateADE_noload = false;
bool fwaitingForDisable = false;
bool fCalibrationMode = false;


__no_init struct 
{
	u32 val;
	u16 crc;
} timeOfWork;

__no_init struct 
{
	u32 val;
	u16 crc;
} telemetry;


/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/