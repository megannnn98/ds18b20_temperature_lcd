/**
  ******************************************************************************
  * @file    processADE_ISR.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Module contain functions to process ISR from ADE7953
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "ade7953.h"
#include "eeCoding.h"
#include "calcCRC.h"

/* Global variables ---------------------------------------------------------*/
extern bool stateADE_noload;
extern struct
{
	uint32_t AWGain;           /* Active energy gain (Current Channel A) */
	uint32_t AVarGain;         /* Reactive energy gain (Current Channel A) */
	uint32_t AVAGain;          /* Apparent energy gain (Current Channel A) */
	uint32_t AWattOs;          /* Active energy offset correction (Current Channel A) */
	uint32_t AVarOs;           /* Reactive energy offset correction (Current Channel A) */
	uint32_t AVAOs;            /* Apparent energy offset correction (Current Channel A) */
	uint32_t AIrmsOs;          /* IRMS offset (Current Channel A) */
	uint32_t VrmsOs;           /* VRMS offset */
	uint32_t PhCalA;           /* Phase calibration register (Current Channel A). This register is in sign magnitude format.
	10 bits: bits 0..8 - value 0,021 degree/LSB; bit 9 - sign */

	uint32_t AIGAIN;          /* AIGAIN IRMS scale */
	uint32_t VGAIN;            /* VGAIN  VRMS scale*/  

	uint32_t DateTime;         /* ���� ���������� � ��������*/
} CalAde7953;

extern struct {
	float Frequency; 
	float V; 
	float I;

	float APower;
	float RPower;
	float APPower;
//	float PowerFactor;
	
} rms;

extern __no_init struct 
{
	u32 val;
	u16 crc;
} telemetry;

struct{
	u8  ZX50;
	u8  StartZX20;  
} adeCnt;

static struct {

	u32 Period_Ade7953_Sum;
	u32 Vrms_Ade7953_Sum;
	u32 Irms_Ade7953_Sum;
	s32 APower_Ade7953_Sum;
	s32 RPower_Ade7953_Sum;
	u32 APPower_Ade7953_Sum;

	u32 Period_Ade7953_Calc;
	u32 Vrms_Ade7953_Calc;
	u32 Irms_Ade7953_Calc;
	s32 APower_Ade7953_Calc;
	s32 RPower_Ade7953_Calc;
	u32 APPower_Ade7953_Calc;
									
} readings;

static struct {
	s32   A_Sum;
	s32   R_Sum;
	s32   AP_Sum;
} instaneousPower;

static struct {
  
	// ** �������� ������� +-
	u32 AEnergy_Ade7953_Sum_Plus;
	u8 Count_TMI_AEnergy_Plus;
	u32 AEnergy_Ade7953_Sum_Minus;
	u8 Count_TMI_AEnergy_Minus;
	
	// ** ���������� ������� +-
	u32 REnergy_Ade7953_Sum_Plus;
	u8 Count_TMI_REnergy_Plus;
	
} energy;


/* Private function prototypes -----------------------------------------------*/
void readoutADE();
void processSecond();
static void ADE_CalcRms(void);











/* Private functions ---------------------------------------------------------*/


/**
  * @brief  ������ ���������� ��������� ����������
  * @retval None
  */
void Calculate_TMI_AEnergy_Plus()
{
	uint8_t tmi = 0;

	// ** �������� ������������ ���������� ��������� ����������
	// ** ���������� ���������� ������� ���������
	// ** ���������� ������� ��������� �� 4000
		
	tmi = energy.Count_TMI_AEnergy_Plus;	
	energy.Count_TMI_AEnergy_Plus = 0;	
	
	
	telemetry.val += tmi;
	if ((telemetry.val/40) > 99999999uL) { telemetry.val = 0uL;}
	telemetry.crc = CRC16((void*)&telemetry.val, sizeof(telemetry.val));

}





/**
  * @brief  ��������� ������ �� ����������
  * @note   ������� ���������� ��������, ����� �������
  * @retval None
  */
void readoutADE()
{
  	u32 tmp = 0;
	volatile u32 xa, xr, xap;

	// ** RMS �������� 
	readings.Period_Ade7953_Sum += ade.read(ADE_REG_PERIOD);
	readings.Vrms_Ade7953_Sum   += ade.read(ADE_REG_VRMS);
	readings.Irms_Ade7953_Sum   += ade.read(ADE_REG_IRMSA);
	
	
	xap = tmp = ade.read(0x210);
	if((tmp & 0x800000) == 0x800000) {
		tmp ^= 0xFFFFFF;
		tmp++;						
	}
	instaneousPower.AP_Sum += tmp;

	xa = tmp  = ade.read(0x212);
	if((tmp & 0x800000) == 0x800000) 
	{
		tmp ^= 0xFFFFFF;
		tmp++;	
	}
	instaneousPower.A_Sum += tmp;

	xr = tmp  = ade.read(0x214);
	if((tmp & 0x800000) == 0x800000) {
		tmp ^= 0xFFFFFF;
		tmp++;						
	}
	instaneousPower.R_Sum += tmp;
	
	
	
	// ** AENERGY 
	// ** ���� ������������� ������� - ������ � ������� �������������
	tmp = ade.read(ADE_REG_AENERGYA);
	if((tmp & 0x800000) == 0x800000) 
	{
		tmp ^= 0xFFFFFF;
		tmp++;
	}
	energy.AEnergy_Ade7953_Sum_Plus += tmp;
	
	// ���� �� �������� ADE_CFxDEN-�
	while (energy.AEnergy_Ade7953_Sum_Plus >= ADE_CFxDEN)
	{
		// ����������� ������� ���������
		energy.Count_TMI_AEnergy_Plus++;
		energy.AEnergy_Ade7953_Sum_Plus -= ADE_CFxDEN;
	}

	
	// RENERGY
	tmp = ade.read(ADE_REG_RENERGYA);
	if((tmp & 0x800000) == 0x800000)
	{
		tmp ^= 0xFFFFFF;
		tmp++;
	}
	energy.REnergy_Ade7953_Sum_Plus += tmp;
	while (energy.REnergy_Ade7953_Sum_Plus >= ADE_CFxDEN)
	{
		energy.Count_TMI_REnergy_Plus++;
		energy.REnergy_Ade7953_Sum_Plus -= ADE_CFxDEN;
	}
	
	
	
	// APENERGY
	// ** ������ �������������
	tmp = ade.read(ADE_REG_APENERGYA);
	if((tmp & 0x800000) == 0x800000)
	{
		tmp ^= 0xFFFFFF;
		tmp++;
	}
	readings.APPower_Ade7953_Sum += tmp;

}


/**
  * @brief  ��������� �������
  * @retval None
  */
void processSecond()
{
	
	readings.Period_Ade7953_Calc = readings.Period_Ade7953_Sum;
	readings.Period_Ade7953_Sum = 0;
	
	readings.Vrms_Ade7953_Calc = readings.Vrms_Ade7953_Sum;
	readings.Vrms_Ade7953_Sum = 0;
	
	readings.Irms_Ade7953_Calc = readings.Irms_Ade7953_Sum;
	readings.Irms_Ade7953_Sum = 0;

	
	readings.APPower_Ade7953_Calc = readings.APPower_Ade7953_Sum;
	readings.APPower_Ade7953_Sum = 0;

	
	ADE_CalcRms();
	
	instaneousPower.AP_Sum  = 0;
	instaneousPower.A_Sum   = 0;
}

/**
  * @brief  ���������� � rms �������� 
  * @retval None
  */
static void ADE_CalcRms(void)
{
	volatile float tmp_measurement = 0.;

	rms.Frequency 	= ((float)ADE_LSB_PERIOD) 			 		/ (float)readings.Period_Ade7953_Calc;
	rms.V	 		= ((float)readings.Vrms_Ade7953_Calc / 50.) / (float)VRMSSCALE_EXTERNAL_DEFAULT;

	if (rms.APower < 0)
	{
		rms.APower = (-1)*rms.APower;
	}
	

	if (true == stateADE_noload)
	{
		rms.I	 		= ((float)readings.Irms_Ade7953_Calc / 50.) / (float)AIRMSSCALE_EXTERNAL_DEFAULT;
		rms.APower  	= ((float)(instaneousPower.A_Sum  )  / 50.) / (float)PWRSCALE_EXTERNAL_DEFAULT;
		rms.RPower  	= ((float)(instaneousPower.R_Sum  )  / 50.) / (float)PWRSCALE_EXTERNAL_DEFAULT;
		rms.APPower  	= ((float)(instaneousPower.AP_Sum )  / 50.) / (float)PWRSCALE_EXTERNAL_DEFAULT;	  	
	}
	else
	{
		rms.I	 		= 0.;
		rms.APower  	= 0.;
		rms.RPower  	= 0.;
		rms.APPower  	= 0.;
	}
}


/**
  * @brief  ��������� ������� ��������, ������������ ���������� � ������ ���������
  * @retval None
  */
void ADE_SetResetStateClearVariables()
{
	
	rms.Frequency 	= 0.;
	rms.V	 		= 0.;
	rms.I	 		= 0.;

	rms.APower  	= 0.;
	rms.RPower  	= 0.;
	rms.APPower  	= 0.;
	
	
	readings.Period_Ade7953_Sum  = 0;
	readings.Vrms_Ade7953_Sum    = 0;
	readings.Irms_Ade7953_Sum    = 0;
	
	energy.AEnergy_Ade7953_Sum_Plus     = 0;
	energy.Count_TMI_AEnergy_Plus       = 0;
	
	energy.REnergy_Ade7953_Sum_Plus     = 0;
	energy.Count_TMI_REnergy_Plus       = 0;

	adeCnt.ZX50       = 0;
	adeCnt.StartZX20  = 0;

	instaneousPower.AP_Sum  = 0;	
	instaneousPower.A_Sum   = 0;	
	instaneousPower.R_Sum   = 0;

	do
	{
		ade.write(0xFE, 0xAD);
		ade.write(0x120, 0x0030);
	}
	while(0x0030 != ade.read(0x120));

	// ZXTOUT : 0x0A77(2679) ~ 200 msec  // ������� �� ����������� ZX 200 ��
	// 0x1652 ~ 40 ms
	ade.write(ADE_REG_ZXTOUT, 0x0A77*4/10);
	// CF1DEN 
	ade.write(ADE_REG_CF1DEN, ADE_CFxDEN); // ����������� �������� ������� ��� ����������
	ade.write(ADE_REG_CF1DEN, ADE_CFxDEN); // ����������� �������� ������� ��� ����������
	// CF2DEN 
	ade.write(ADE_REG_CF2DEN, ADE_CFxDEN); // ����������� �������� ������� ��� ����������
	ade.write(ADE_REG_CF2DEN, ADE_CFxDEN); // ����������� �������� ������� ��� ����������
	
	// �������� �������� ������ � � 16 ���
	#define GAIN_16		4
	#define GAIN_22		5
	ade.write(ADE_REG_PGA_IA, GAIN_16);
	



	ade.write(ADE_REG_AWGAIN,   CalAde7953.AWGain);   // ��������� �������� ��������
	ade.write(ADE_REG_AVARGAIN, CalAde7953.AVarGain); // ��������� ���������� ��������
	ade.write(ADE_REG_AVAGAIN,  CalAde7953.AVAGain);  // ��������� ������ ��������
	
	ade.write(ADE_REG_AIRMSOS,  CalAde7953.AIrmsOs);  // IRMS �����
	ade.write(ADE_REG_VRMSOS,   CalAde7953.VrmsOs);   // VRMS �����
	ade.write(ADE_REG_AWATTOS,  CalAde7953.AWattOs);  // ��������� ������ �������� �������� �
	ade.write(ADE_REG_AVAROS,   CalAde7953.AVarOs);   // ��������� ������ ���������� �������� �
	ade.write(ADE_REG_AVAOS,    CalAde7953.AVAOs);    // ��������� ������ ������ �������� �
	ade.write(ADE_REG_PHCALA,   CalAde7953.PhCalA);   // ���������� ���� �

	ade.write(ADE_REG_AIGAIN,   CalAde7953.AIGAIN);   // ���������� ����
	ade.write(ADE_REG_VGAIN,    CalAde7953.VGAIN);    // ���������� ����������

	ade.write(ADE_REG_CFMODE, 0x010);  		// CF1 ��������������� ����������, CF2 ��������������� ��������
											// ������ ���������� ��������
	
	// ** CONFIG
	// ** �������� HPF (��� ������), 
	// ** Zero-crossing ���������� - �� �������� �������
	ade.write(ADE_REG_CONFIG, 0x1004); // ������� ������������ (1 << 12) | (1 << 2)
	// LCYCMODE
	// ��� ���� ��������� ���������, ��� �� �����������
	ade.write(ADE_REG_LCYCMODE, 0x040); // (1 << 6)

	
	ade.write(ADE_REG_AP_NOLOAD,  ADE_NOLOAD_VAL);  // ������� ���������� �������� �������� �������� 
	ade.write(ADE_REG_VAR_NOLOAD, ADE_NOLOAD_VAL);  // ������� ���������� �������� ���������� �������� 
	ade.write(ADE_REG_VA_NOLOAD,  ADE_NOLOAD_VAL);  // ������� ���������� �������� ������ �������� 

	// DISNOLOAD
	// �������� ���� �� �������� ��� ��������, ���������� � ������ ��������
	ade.write(ADE_REG_DISNOLOAD, 0);
	// ������� IRQENA
	// �������� ���������� ����� ��������� ����� (1 << 20), 
	// ����������� ���� ZXV � ������� �� ������������� ���� ZXTO (1 << 15)| (1 << 14)
	ade.write(ADE_REG_IRQENA, 0x10C000);
}



/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/



















