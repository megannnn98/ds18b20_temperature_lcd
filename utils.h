
/**
  ******************************************************************************
  * @file    utils.h
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Common macros
  ******************************************************************************  
  */
#pragma once

//#define DEBUG
#ifdef DEBUG
#else
	#define RELEASE
#endif


/* Includes ------------------------------------------------------------------*/
#include "stm8l15x.h"
#include <stdbool.h>
#include "pt.h"
#include "pt-sem.h"
#include "HAL.h"
#include "eeCoding.h"


/* Private define ------------------------------------------------------------*/


#define CLK_16MHz            CLK->CKDIVR = 0x00
#define CLK_8MHz             CLK->CKDIVR = 0x01
#define CLK_4MHz             CLK->CKDIVR = 0x02
#define CLK_2MHz             CLK->CKDIVR = 0x03
#define CLK_1MHz             CLK->CKDIVR = 0x04
#define CLK_05MHz            CLK->CKDIVR = 0x05
#define CLK_025MHz           CLK->CKDIVR = 0x06
#define CLK_0125MHz          CLK->CKDIVR = 0x07

#define NULL                 0L






#define CREATE_LONG_VAL(a,b,c) 		 ((((u32)a) << 16) + (((u32)b) << 8) + (((u32)c) << 0))		


#define MS_IN_SECOND		 (1000uL)
#define MS_IN_MINUTE		 (60uL * MS_IN_SECOND) //          60_000
#define MS_IN_HOUR			 (60uL * MS_IN_MINUTE) //  	    3_600_000
#define MS_IN_DAY			 (24uL * MS_IN_HOUR)   // 	   86_400_000
#define MS_IN_YEAR			 (365uL* MS_IN_DAY)    //  31_536_000_000
#define MS_IN_30_YEARS		 (30uL * MS_IN_YEAR)   // 946_080_000_000 5 ����


// ** Watch Dog **************************************

#define IWDG_RESET IWDG->KR=IWDG_KEY_REFRESH


#define TXBUF_SIZE 10

enum
{
	GENERAL_TIMER = 0,
	SEC_TIMER,
	USART_TIMER,
	SELF_PROPELLED_TIMER,
	ADE_DEAD_TIMER,
	SAVING_TIMER,
	TIME_OF_WORK_TIMER,
    
  MAX_TIMERS
};

enum {
    PR_MAX = 0,
    PR_HIGH,
    PR_MEDIUM,
    PR_LOW,
    PR_LOWEST,
};


enum
{
  GENERAL_MSG = 0, 
  ADE_ISR_MSG,
  ADE_RESET_MSG,
  MAX_MSG
};


#define PT_DELAY_MS(timer, ms)	do { tim.reset(timer); \
								 PT_WAIT_WHILE(pt, tim.get(timer) < ms); } while(0)

#define PT_STATE_INIT() 	 static pt_t val_pt = {.lc = 0}; \
							 static pt_t *pt = &val_pt;

// ������������ ������������ � ������������
//#define PT_INIT(pt, priority)   LC_INIT((pt)->lc ; (pt)->pri = priority)
//PT_PRIORITY_INIT
#define PT_RATE_INIT() char pt_pri_count = 0;
// maitain proority frame count
//PT_PRIORITY_LOOP ������������ ������� ���������
#define PT_RATE_LOOP() do { pt_pri_count = (pt_pri_count+1) & 0xf; } while(0);
// ��������� ����� � �����������
//PT_PRIORITY_SCHEDULE
// 5 �������
// �������� 0 ���� ������ -- ���������� ������ ����
// ��������� 1 -- ������  2 ����� 
// ��������� 2 -- ������  4 ������
// ��������� 3 -- ������  8 ������
// ��������� 4 -- ������ 16 ������
#define PT_PR_EXECUTE(rate, code) \
   do { if((PR_MAX     == rate) | \
           (PR_HIGH    == rate && (0 == (pt_pri_count & 0x01))) | \
           (PR_MEDIUM  == rate && (0 == (pt_pri_count & 0x03))) | \
           (PR_LOW     == rate && (0 == (pt_pri_count & 0x07))) | \
           (PR_LOWEST  == rate && (0 == (pt_pri_count & 0x0F)))) \
           {code;}; } while(0);


#define EXECUTE_AND_IF_ERR_RETURN(code) \
   do { if (ERROR_WHILE_TRUE == code) return ERROR_WHILE_TRUE; } while(0);
   

/* Global variables ---------------------------------------------------------*/

/** @defgroup Timer_driver
  * @{
  */
struct Timer_driver
{
	u32 (*get)(const u8 timer);
	void (*reset)(const u8 timer);
	void (*init)();
};

/**
  * @}
  */
extern const struct Timer_driver tim;


/**
  * @{ @brief  ��������� ������� MSG.
*/
struct Msg_driver
{
	void (*init)();
    void (*process)(); 
    u8   (*set)(const u8 imsg, void* iparamPtr, u16 iparamSize);
    u8   (*repeat)(const u8 imsg);
    
	bool (*getVal)(const u8 imsg)	 ;
	void* (*getPtr)(const u8 imsg) ;
	u16   (*getSize)(const u8 imsg);
};
/**
  * @}
  */

extern const struct Msg_driver msg;


extern bool fCalibrationMode;
/* Private function prototypes -----------------------------------------------*/

void saveTelemetry(void);
MEM_STATUS_t wrapperWrite(u32 addres, void* data);
MEM_STATUS_t wrapperRead(uint16_t adress, void* dataPtr, uint32_t defaultVal);


/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/
