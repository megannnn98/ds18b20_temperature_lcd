
/**
  ******************************************************************************
  * @file    thread_ade.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Perform self propelled, save readings, and count time of work Threads
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "lcd.h"
#include "eeCoding.h"
#include "calcCRC.h"

/* Global variables ---------------------------------------------------------*/
extern bool stateADE_noload;
extern bool fwaitingForDisable;
// ** ������ ������� 04 00 + crc 
// ** 				 04 01 + crc


extern __no_init struct 
{
	u32 val;
	u16 crc;
} timeOfWork;
extern __no_init struct 
{
	u32 val;
	u16 crc;
} telemetry;



/* Private functions ---------------------------------------------------------*/

/**
  * @brief  ����� �������� �� �������
  * @note   �������� �� �������
			���� � ade ��������� ���� ���������� �������� 
			������ 
  * @retval None
  */
PT_THREAD(thread_selfPropelled())
{
	// ** ������� �������� ��� ����������� ���������� �������� 
	
	static const u8 TABLE_NOLOAD[4] = {1, 2, 4, 2}; // ** 001 010 100
	PT_STATE_INIT();
	PT_BEGIN(pt);
  	static u8 tmp = 0;
	tmp = 0;
		
	while(1)
	{
	  
		lcd.text(SHOW, 2, S3, S7);
		
	  	if (true == stateADE_noload) {
			
			lcd.num_channel(TABLE_NOLOAD[tmp]);
			(tmp > sizeof(TABLE_NOLOAD)-2) ? tmp = 0 : tmp++;
		}
		else {
			lcd.num_channel(CHANEL_CLEAR);
		}
		
		PT_DELAY_MS(SELF_PROPELLED_TIMER, 500);
	}
	
	PT_END(pt);
}




/**
  * @brief  � ������ ��� � 160 ����� ����������� ������ ���� ��� ����������
  * @retval None
  */
PT_THREAD(thread_saveReadings())
{
	PT_STATE_INIT();
	PT_BEGIN(pt);
	
	while(1)
	{
	#ifdef RELEASE
		PT_DELAY_MS(SAVING_TIMER, 160uL*MS_IN_MINUTE);
	#else
		PT_DELAY_MS(SAVING_TIMER,   1uL*MS_IN_MINUTE);
	#endif
		
		if (false == fwaitingForDisable)
		{
			// ** �������� ����� ������ � ����������
			saveTelemetry();
			wrapperWrite(I_EE_TIME_OF_WORK, &timeOfWork.val);
		}

	}
	PT_END(pt);
}


extern struct {
	float Frequency; 
	float V; 
	float I;

	float APower;
	float RPower;
	float APPower;
//	float PowerFactor;
	
} rms;

/**
  * @brief  ���� ������� ������, � ��������
  * @retval None
  */
PT_THREAD(thread_countTimeOfWork_showData())
{
	PT_STATE_INIT();
	PT_BEGIN(pt);

	
	while(1)
	{
    lcd.text(HIDE, 2, POINT, W);
		lcd.show_long((s32)(telemetry.val/40), false, false);
		lcd.text(SHOW, 4, POINT, k, W, h);

		PT_DELAY_MS(TIME_OF_WORK_TIMER, 2*MS_IN_SECOND);
    
    lcd.text(HIDE, 4, POINT, k, W, h);
		lcd.show_float(rms.APower*1000, false, false);
		lcd.text(SHOW, 2, POINT, W);
		
		PT_DELAY_MS(TIME_OF_WORK_TIMER, 2*MS_IN_SECOND);
    
		
		timeOfWork.val += 4;
		timeOfWork.crc = CRC16((void*)&timeOfWork.val, sizeof(timeOfWork.val));
	}
	PT_END(pt);
}



  
/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/
	
	