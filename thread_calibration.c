
/**
  ******************************************************************************
  * @file    thread_ade.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Perform Calibration Thread
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "calibration.h"
#include "calcCRC.h"

/* Private define ------------------------------------------------------------*/
#define IS_USART_ERRORS ( (USART_SR_FE == (USART1->SR & USART_SR_FE)) \
						||(USART_SR_NF == (USART1->SR & USART_SR_NF)) \
						||(USART_SR_PE == (USART1->SR & USART_SR_PE)))

/* Private variables ---------------------------------------------------------*/

pt_sem_t symRX_sem;

/* Global variables ---------------------------------------------------------*/

extern struct {
  
	u8 buf_RX[10];
	u8 buf_TX[TXBUF_SIZE];
  	u8 counterRX;
	u8 counterTX;
  	u8 bytesForSend;
} usart;


/* Private function prototypes -----------------------------------------------*/

void USART_calibr_Init();
void processPkt();
void RS_485_AddCRC_Cal();
void RS_485_SendERR(u8 ErrCode);



/* Private functions ---------------------------------------------------------*/

void clearRX(){	for(u8 i = 0; i < sizeof(usart.buf_RX); usart.buf_RX[i++] = 0);	usart.counterRX = 0;}
void clearTX(){	for(u8 i = 0; i < sizeof(usart.buf_TX); usart.buf_TX[i++] = 0);	usart.counterTX = 0;}


/**
  * @brief  ���������� �� ��������
  * @retval None
  */
INTERRUPT_HANDLER(USART_CALIBR_TX_IRQHandler, 27)
{
    while(1);
}

/**
  * @brief  ���������� �� ������
  * @retval None
  */
INTERRUPT_HANDLER(USART_CALIBR_RX_IRQHandler, 28)
{
    while(1);
}




/************************END OF FILE****/