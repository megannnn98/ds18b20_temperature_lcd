/**
  ******************************************************************************
  * @file    lcd.h
  * @author  Karatanov M.N.
  * @version 0.1
  * @date    18-June-2015
  * @brief   lcd functions header
  ******************************************************************************
*/
#pragma once

#include "time.h"
#include "stm8l15x_rtc.h"
//** Special symbols	
enum{
    T1	  = 0,
    T2    = 1,
    L1	  = 2,
    L2	  = 3,
    L3	  = 4,
    SUM	  = 5,
    MAX	  = 6,

    S1	  = 7,
    S2	  = 8,
    S3	  = 9,
    S4	  = 10,
    S5	  = 11,
    S6	  = 12,
    S7	  = 13,
    S8	  = 14,	
    S9	  = 15,

    S10	  = 16,
    S11	  = 17,
    S12	  = 18,
    S13	  = 19,
    S14	  = 20,
    S15	  = 21,

    V	  = 22,
    k	  = 23,
    M	  = 24,
    r	  = 25,
    h	  = 26,
    P1	  = 27,
    P2	  = 28,
    P3	  = 29,
    P4	  = 30,
    H 	  = 31,

	W	  = 32,
	A	  = 33,
	PO	  = 34,

    LINE  = 255,
};
// ������ ��� ������� 1
enum{
    E  = 10,	
};
// ������ ��� ������� 2
enum{
    r2  = 11,	
};
// ������ ��� ������� 3
enum{
    r3  = 10,	
};
// ������ ��� ������� 4
enum{
    o  = 10,	
};
// ������ ��� ������� 5
enum{
    r5  = 10,	
};



// ������ ��� ������� 9
enum{
    INDUCTANCE  = 10,
    CAPACITANCE = 11,
    FREQUENCY   = 12,
	MINUS		= 13,
	EXPORT		= 14,	
};
// ������ ��� ������� 2
enum{
    C  = 10,	
};
// ������ ��� ������� 7
enum{
    GRADUS  	= 10,
	YEAR    	= 11,
};
// ������ ��� ������� 8
enum{
    CELSIUS  	= 10,
};

enum{
    LCD_CLK  	= 3,
    RTC_CLK  	= 2,
};

// �������������� ����������� 

    #define CLOCK 			S2
    #define MONEY 			S1
    #define POINT 			P3
    #define RELAY 			S12
    #define ANTENNA			S8
    #define NUM_INSERETING	S10
		

#define CHANEL_CLEAR	10

#define HIDE	(0)
#define SHOW	(!HIDE)




struct LCD_driver
{
	void (*init)();
	void (*num_channel)(const uint8_t value);
	void (*show_left_int)(u32 data);
	void (*show_float)(float data, const bool fShowZeroes, const bool fShowMinusObligatorily);
	void (*show_long)(s32 data, const bool fShowZeroes, const bool fShowMinusObligatorily);
	void (*show_bcd)(s32 data, const bool fShowZeroes, const bool fShowMinusObligatorily);
	void (*show_lines)();
//	void (*show_line_pos)(const u8 pos);
//	void (*show_float)(float dataf, const bool fShowZeroes, const bool fShowMinusObligatorily);
	
//	void (*deinit)();
//	void (*show_symbol)(const uint8_t value);
//	void (*hide_symbol)(const uint8_t value);
	void (*all)();
	void (*clear)();
	void (*clear_data)();
//	void (*get_ram_14_bytes)(u8* iramPtr);
//	void (*set_ram_14_bytes)(u8* iramPtr);
	void (*show_val_pos)(const u8 value, u8 position);
	void (*text)(const bool showFlag, u16 num, ...);
	void (*show_date)(const RTC_DateTypeDef* DateStructure);
	void (*show_time)(const RTC_TimeTypeDef* TimeStructure);

};
extern const struct LCD_driver lcd;
/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/
