
/**
  ******************************************************************************
  * @file    eeCoding.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Module contain fuctions for read and write EEPROM
  ******************************************************************************  
  */


// ** I_EE_NUMPULT

/* Includes ------------------------------------------------------------------*/
#include "eeCoding.h"
#include "stdbool.h"
#include "calcCRC.h"

/* Private define ------------------------------------------------------------*/
							 


#define MEM_readByte 		FLASH_ReadByte
#define taskENTER_CRITICAL();				
#define taskEXIT_CRITICAL();
#define MEM_BLOCK_SIZE		FLASH_BLOCK_SIZE

/* Private macro -------------------------------------------------------------*/
#define EEPROM_LOCK     do {  FLASH->IAPSR &= (uint8_t)FLASH_MemType_Data;    \
                              FLASH->IAPSR &= (uint8_t)FLASH_MemType_Program; \
                           }while(0)
                             
                      
#define EEPROM_UNLOCK   do {  FLASH->PUKR = FLASH_RASS_KEY1; \
                              FLASH->PUKR = FLASH_RASS_KEY2; \
                              FLASH->DUKR = FLASH_RASS_KEY2; \
                              FLASH->DUKR = FLASH_RASS_KEY1; \
                           }while(0)


							 
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Read amount of data from specified adress
  * @param  ��������� �� ������ ���� ������
  * @param  ����� �������
  * @retval None
  */
static void MEM_readPkt(u16 startAddr, u8* ptr, const u16 len)
{
	for(u16 i = 0 ; i < len ; i++)
	{
		ptr[i] = MEM_readByte(startAddr + i);		
	}
}

/**
  * @brief  Write amount of data from specified adress
  * @param  ��������� �� ������ ���� ������
  * @param  ����� �������
  * @retval ������ ���������� � ���� �� ������
  */
static void MEM_writePkt(u16 startAddr, u8* ptr, const u16 len)
{
	
    EEPROM_UNLOCK;
		
	for(u16 i = 0 ; i < len ; i++)
	{
		FLASH_ProgramByte(startAddr + i, ptr[i]);
		while(SET == FLASH_GetFlagStatus(FLASH_FLAG_EOP));
	}
	
    EEPROM_LOCK;
}


/**
  * @brief  Write 4 bytes to eeprom with CRC
  * @param  adress
  * @param  ptr to 4 bytes
  * @retval None
  */
static void MEM_write32withCRC(u16 adress16, void* dataPtr)
{
	u16 CRC_calc = 0;
	u8 t = 0;
	
	CRC_calc = CRC16(dataPtr, 4);
	
	MEM_writePkt(adress16, dataPtr, 4);
	
	t = BYTE_0(CRC_calc);	
	MEM_writePkt(adress16 + 4, &t, 1);
	
	t = BYTE_1(CRC_calc);	
	MEM_writePkt(adress16 + 5, &t, 1);
}

/**
  * @brief  Read 4 bytes to eeprom and check CRC
  * @param  adress
  * @param  ptr to 4 bytes
  * @retval result ok or not
  */
static MEM_STATUS_t MEM_read32withCRC(u16 adress16, void* dataPtr)
{
	u16 CRC_calc = 0, CRC_read = 0;
	
	MEM_readPkt(adress16, dataPtr, 4);
	CRC_read = ((u16)MEM_readByte(adress16+5) << 8) + ((u16)MEM_readByte(adress16+4));
	
	
	CRC_calc = CRC16(dataPtr, 4);

	
	if(CRC_calc == CRC_read)
	{
	  	return STATUS_OK;
	}
	return STATUS_ERROR;
}

/**
  * @brief  ������ ���� ����� 4 ������� ����������
  * @param  adress
  * @param  ptr to 4 bytes
  * @retval result ok or not
  */
static MEM_STATUS_t MEM_writeDataTwoCopyes(u16 adressInMem, void* pDataForWrite)
{
  	u32 iTmp1ForRead = 0UL, iTmp2ForRead = 0UL;
  
  	bool flag1ok = false;
  	bool flag2ok = false;

	
	// ** ������ ����� ���������� 
	if (STATUS_OK == MEM_read32withCRC(adressInMem, &iTmp1ForRead))
	{
		flag1ok = true;
	}
	if (STATUS_OK == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &iTmp2ForRead))
	{
		flag2ok = true;
	}	
	
	// ** CRC ����� ���������
	if ((true == flag1ok) && (true == flag2ok))
	{
		// ������� ������ �� ������� ���� ���������� ������
		if ((iTmp1ForRead == iTmp2ForRead) && (iTmp1ForRead == *(u32*)pDataForWrite))
		{
		}
		// ����� ������
		else
		{
			// ** ������� - ������ ������, ����� � ������ 
			MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
			if (1 == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &iTmp1ForRead))
			{
				MEM_write32withCRC(adressInMem, pDataForWrite);
			}
			else
			{
				return STATUS_ERROR;
			}
		}
	}
  
	// ** ������ ������
	else if ((false == flag1ok) && (true == flag2ok))
	{
		// ** ������� - ������ ������, ����� � ������ 
		MEM_write32withCRC(adressInMem, pDataForWrite);
		if (STATUS_OK == MEM_read32withCRC(adressInMem, &iTmp1ForRead))
		{
			MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
		}
		else
		{
			return STATUS_ERROR;
		}
	}

	// ** ������ ������
	else if ((true == flag1ok) && (false == flag2ok))
	{
		// ** ������� - ������ ������, ����� � ������ 
		MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
		if (STATUS_OK == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &iTmp1ForRead))
		{
			MEM_write32withCRC(adressInMem, pDataForWrite);
		}
		else
		{
			return STATUS_ERROR;
		}
	}	
	// ** ��� �����
	else
	{
		// ** ����� ���� ��� �����
		MEM_write32withCRC(adressInMem, pDataForWrite);
		MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
		
		return STATUS_ERROR;
	}
		
	return STATUS_OK;
}


/**
  * @brief  ������ ����� ���������� ����� 4� ������� ����������
  * @param  adress
  * @param  ptr to 4 bytes
  * @retval result ok or not
  */
static MEM_STATUS_t MEM_readDataTwoCopyes(u16 adressInMem, void* dataPtr, u32 defaultVal)
{
	u32   data_r[2]    = {0ul};
	bool iFlagERROR[2] = {false, false};
	
	// ** ������ ������ � ��������� �� CRC 
	// ** ���� ������ - ���������� �����
	
	if (STATUS_ERROR == MEM_read32withCRC(adressInMem, &data_r[0]))
	{
		iFlagERROR[0] = true;
	}
	if (STATUS_ERROR == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &data_r[1]))
	{
		iFlagERROR[1] = true;
	}
	
	// ** ��� ����� ���������� 
	if ((false == iFlagERROR[0]) && (false == iFlagERROR[1]))
	{
		*(u32*)dataPtr = data_r[0];
		
		return STATUS_OK;
	}
	// ** ������ ����� ��������, ��������� ������ ���������� � ��� �����
	else if ((false == iFlagERROR[0]) && (true == iFlagERROR[1]))
	{
		MEM_writeDataTwoCopyes(adressInMem, &data_r[0]);
		*(u32*)dataPtr = data_r[0];
		
		return STATUS_WAS_ERROR;
	}
	// ** ������ ����� ��������, ��������� ������ ���������� � ��� �����
	else if ((true == iFlagERROR[0]) && (false == iFlagERROR[1]))
	{
		MEM_writeDataTwoCopyes(adressInMem, &data_r[1]);
		*(u32*)dataPtr = data_r[1];
		
		return STATUS_WAS_ERROR;
	}	
	// ** ��� �������
	MEM_writeDataTwoCopyes(adressInMem, &defaultVal);
	*(u32*)dataPtr = defaultVal;

	return STATUS_ERROR;	
}
/**
  * @brief  ��������� ������� MEM.
*/
const struct Mem_driver mem = 
{
	.writeDataTwoCopyes = MEM_writeDataTwoCopyes,
	.readDataTwoCopyes  = MEM_readDataTwoCopyes,
};

/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/