
/**
  ******************************************************************************
  * @file    lcd.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Module contain fuctions for working with lcd
  ******************************************************************************  
  */


/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "math.h"
#include "lcd.h"
#include "stdarg.h"

/* Private define ------------------------------------------------------------*/
// ** LCD FRQ

	#define PS               	4 
	#define DIV              	0     

// ** LCD CR1

	#define BLINK   			6
    #define BLINKF 				3
    #define DUTY    			1
    #define BIAS    			0

    #define DUTY_1_4         	(3<<DUTY)
    #define BIAS_1_3         	(0<<BIAS)

	#define BLINK_INACTIVE     	(0<<BLINK)
	#define BLINK_1PIXEL       	(1<<BLINK)
	#define BLINK_UP_TO_4PIXEL 	(2<<BLINK)
	#define BLINK_ALL_PIXELS   	(3<<BLINK)


	#define BLINKF_8         	(0<<BLINKF)
	#define BLINKF_16        	(1<<BLINKF)
	#define BLINKF_32        	(2<<BLINKF)
	#define BLINKF_64        	(3<<BLINKF)
	#define BLINKF_128       	(4<<BLINKF)
	#define BLINKF_256       	(5<<BLINKF)
	#define BLINKF_512       	(6<<BLINKF)
	#define BLINKF_1024      	(7<<BLINKF)

//** LCD CR2

	#define PON              	5
	#define HIGH_DRIVE_EN    	4
	#define CONTRAST         	1
	#define VSEL             	0

	#define CLK_PS_0 			(0<<PON)
	#define CLK_PS_1 			(1<<PON)
	#define CLK_PS_2 			(2<<PON)
	#define CLK_PS_3 			(3<<PON)
	#define CLK_PS_4 			(4<<PON)
	#define CLK_PS_5 			(5<<PON)
	#define CLK_PS_6 			(6<<PON)
	#define CLK_PS_7 			(7<<PON)

	#define HDRIVER_DIS  		(0<<HIGH_DRIVE_EN)
	#define HDRIVER_EN   		(1<<HIGH_DRIVE_EN)

	#define VLCD0        (0<<CONTRAST)
	#define VLCD1        (1<<CONTRAST)
	#define VLCD2        (2<<CONTRAST)
	#define VLCD3        (3<<CONTRAST)
	#define VLCD4        (4<<CONTRAST)
	#define VLCD5        (5<<CONTRAST)
	#define VLCD6        (6<<CONTRAST)
	#define VLCD7        (7<<CONTRAST)

	#define INT_SOURCE   (7<<VSEL)
	#define EXT_SOURCE   (7<<VSEL)

//** LCD CR3

	#define LCDEN            6
	#define SOFIE            5
	#define SOF              4
	#define SOFC             3 
	#define DEAD             0


//** Table 

    #define RAM_0_S2    (1<<7)
    #define RAM_0_L1    (1<<6)
    #define RAM_0_3A    (1<<5)
    #define RAM_0_3F    (1<<4)
    #define RAM_0_2A    (1<<3)
    #define RAM_0_2F    (1<<2)
    #define RAM_0_T1    (1<<1)
    #define RAM_0_9D    (1<<0)
    
    
    #define RAM_1_8A    (1<<7)
    #define RAM_1_S3    (1<<6)
    #define RAM_1_T2    (1<<5)
    #define RAM_1_10D   (1<<4)
    #define RAM_1_5A    (1<<3)
    #define RAM_1_5F    (1<<2)
    #define RAM_1_4A    (1<<1)
    #define RAM_1_4F    (1<<0)
    

    #define RAM_2_S8    (1<<7)
    #define RAM_2_6F    (1<<6)
    #define RAM_2_6A    (1<<5)
    #define RAM_2_V     (1<<4)
    #define RAM_2_S13   (1<<3)
    #define RAM_2_7F    (1<<2)
    #define RAM_2_7A    (1<<1)
    #define RAM_2_8F    (1<<0)


    #define RAM_3_2B    (1<<7)
    #define RAM_3_2G    (1<<6)
    #define RAM_3_9C    (1<<5)
    #define RAM_3_9E    (1<<4)
    #define RAM_3_1A    (1<<1)
    #define RAM_3_1F    (1<<0)


    #define RAM_4_5B    (1<<7)    
    #define RAM_4_5G    (1<<6)    
    #define RAM_4_4B    (1<<5)
    #define RAM_4_4G    (1<<4)    
    #define RAM_4_S1    (1<<3)  
    #define RAM_4_L2    (1<<2)
    #define RAM_4_3B    (1<<1)    
    #define RAM_4_3G    (1<<0)    


    #define RAM_5_S14   (1<<7)
    #define RAM_5_7G    (1<<6)    
    #define RAM_5_7B    (1<<5)        
    #define RAM_5_8G    (1<<4)    
    #define RAM_5_8B    (1<<3)    
    #define RAM_5_S4    (1<<2)    
    #define RAM_5_10C   (1<<1)    
    #define RAM_5_10E   (1<<0)    
    

    #define RAM_6_1B    (1<<5)
    #define RAM_6_1G    (1<<4)
    #define RAM_6_S9    (1<<3)
    #define RAM_6_6G    (1<<2)
    #define RAM_6_6B    (1<<1)
    #define RAM_6_k     (1<<0)


    #define RAM_7_MAX   (1<<7)
    #define RAM_7_L3    (1<<6)
    #define RAM_7_3C    (1<<5)
    #define RAM_7_3E    (1<<4)
    #define RAM_7_2C    (1<<3)
    #define RAM_7_2E    (1<<2)
    #define RAM_7_9B    (1<<1)
    #define RAM_7_9G    (1<<0)
    

    #define RAM_8_8C    (1<<7)
    #define RAM_8_S5    (1<<6)
    #define RAM_8_10B   (1<<5)
    #define RAM_8_10G   (1<<4)
    #define RAM_8_5C    (1<<3)
    #define RAM_8_5E    (1<<2)
    #define RAM_8_4C    (1<<1)
    #define RAM_8_4E    (1<<0)

    
    #define RAM_9_S10   (1<<7)
    #define RAM_9_6E    (1<<6)
    #define RAM_9_6C    (1<<5)
    #define RAM_9_M     (1<<4)
    #define RAM_9_S15   (1<<3)
    #define RAM_9_7E    (1<<2)
    #define RAM_9_7C    (1<<1)
    #define RAM_9_8E    (1<<0)


    #define RAM_10_P1   (1<<7)
    #define RAM_10_2D   (1<<6)
    #define RAM_10_9A   (1<<5)
    #define RAM_10_9F   (1<<4)
    #define RAM_10_1C   (1<<1)
    #define RAM_10_1E   (1<<0)
    

    #define RAM_11_5D   (1<<6)
    #define RAM_11_P4   (1<<5)
    #define RAM_11_4D   (1<<4)
    #define RAM_11_SUM  (1<<3)
    #define RAM_11_P2   (1<<1)
    #define RAM_11_3D   (1<<0)
    

    #define RAM_12_r    (1<<7)
    #define RAM_12_7D   (1<<6)
    #define RAM_12_h    (1<<5)
    #define RAM_12_8D   (1<<4)
    #define RAM_12_S7   (1<<3)
    #define RAM_12_S6   (1<<2)
    #define RAM_12_10A  (1<<1)
    #define RAM_12_10F  (1<<0)  


    #define RAM_13_1D   (1<<4)
    #define RAM_13_S11  (1<<3)
    #define RAM_13_6D   (1<<2)
    #define RAM_13_P3   (1<<1)
    #define RAM_13_S12  (1<<0)


/* Private function prototypes -----------------------------------------------*/

static void lcd_show_lines();
//static void lcd_show_line_pos(const u8 pos);
static void lcd_show_zeroes();
static void lcd_clear();

/* Private functions ---------------------------------------------------------*/


/**
  * @brief  ������� ��������� LCD �����������
  * @param  None
  * @retval None
  */
static void lcd_init()
{  	
    //������ ���� �� RTC
    //������� ��������� RTC	
	CLK_PeripheralClockConfig(CLK_Peripheral_LCD, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
	CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1);
	
    //Duty = 1/4, Blink Freq = LCDclk/256
    //������������� = 6 (�� 7)
    //��� � deadtime = 2 
    //LCDclk = 32768/2/(2^1*16) = 512Hz
	
	LCD_Init(LCD_Prescaler_2, LCD_Divider_16, LCD_Duty_1_4, LCD_Bias_1_3, LCD_VoltageSource_External);
	LCD_DeadTimeConfig(LCD_DeadTime_2);
	LCD_HighDriveCmd(ENABLE);
	LCD_PulseOnDurationConfig(LCD_PulseOnDuration_0);
	LCD_ContrastConfig((LCD_Contrast_TypeDef)LCD_Contrast_3V3);
	
	LCD_BlinkConfig(LCD_BlinkMode_Off, LCD_BlinkFrequency_Div1024);
	
	LCD_Cmd(ENABLE);
	
    //������������� ��� ��������:

	LCD_PortMaskConfig(LCD_PortMaskRegister_0, 0xFF);
	LCD_PortMaskConfig(LCD_PortMaskRegister_1, 0xFF);
	LCD_PortMaskConfig(LCD_PortMaskRegister_2, 0xFF);
	LCD_PortMaskConfig(LCD_PortMaskRegister_3, 0x03);
	  
    lcd_clear();
}

/**
  * @brief  ������� ���
  * @param  None
  * @retval None
  */
static void lcd_clear()
{
    for(uint8_t i = 0; i < 14; i++)
        LCD->RAM[i] = 0x00;	
}
/**
  * @brief  ������� ���
  * @param  None
  * @retval None
  */
static void lcd_all()
{
    for(uint8_t i = 0; i < 14; i++)
        LCD->RAM[i] = 0xFF;	
}
/*
// ** ���������� ��������� ��� 
// ** ������� ������ - ������ �� 14 ���� � ������� ���������� �������� RAM ���
static void lcd_get_ram_14_bytes(u8* iramPtr)
{
    for(uint8_t i = 0; i < 14; i++)
	{
  		iramPtr[i] = LCD->RAM[i];
	}
}
// ** ��������� ��������� ��� 
// ** ������� ������ - ������ �� 14 ���� �� �������� ������� �������� RAM ���
static void lcd_set_ram_14_bytes(u8* iramPtr)
{
    for(uint8_t i = 0; i < 14; i++)
	{
  		LCD->RAM[i] = iramPtr[i];
	}
}*/


/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 1
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos1(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] |= RAM_10_1E;        
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  &= ~RAM_6_1G;
      break;
      case 1:
        LCD->RAM[3]  &= ~RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] &= ~RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  &= ~RAM_3_1F;
        LCD->RAM[6]  &= ~RAM_6_1G;
      break;
      case 2:
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] &= ~RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] |= RAM_10_1E;        
        LCD->RAM[3]  &= ~RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;    
      break;
      case 3:
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  &= ~RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      case 4:
        LCD->RAM[3]  &= ~RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] &= ~RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      case 5:
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  &= ~RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      case 6:
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  &= ~RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] |= RAM_10_1E;        
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      case 7:
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] &= ~RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  &= ~RAM_3_1F;
        LCD->RAM[6]  &= ~RAM_6_1G;
      break;
      case 8: 
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] |= RAM_10_1E;        
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;        
      case 9: 
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  |= RAM_6_1B;
        LCD->RAM[10] |= RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      case E: 
        LCD->RAM[3]  |= RAM_3_1A;
        LCD->RAM[6]  &= ~RAM_6_1B;
        LCD->RAM[10] &= ~RAM_10_1C;
        LCD->RAM[13] |= RAM_13_1D;
        LCD->RAM[10] |= RAM_10_1E;
        LCD->RAM[3]  |= RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      case LINE:
        LCD->RAM[3]  &= ~RAM_3_1A;
        LCD->RAM[6]  &= ~RAM_6_1B;
        LCD->RAM[10] &= ~RAM_10_1C;
        LCD->RAM[13] &= ~RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  &= ~RAM_3_1F;
        LCD->RAM[6]  |= RAM_6_1G;
      break;
      default:
        LCD->RAM[3]  &= ~RAM_3_1A;
        LCD->RAM[6]  &= ~RAM_6_1B;
        LCD->RAM[10] &= ~RAM_10_1C;
        LCD->RAM[13] &= ~RAM_13_1D;
        LCD->RAM[10] &= ~RAM_10_1E;        
        LCD->RAM[3]  &= ~RAM_3_1F;
        LCD->RAM[6]  &= ~RAM_6_1G;         
      break;
  }
}

/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 2
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos2(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  |= RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  &= ~RAM_3_2G;
      break;    
      case 1: 
        LCD->RAM[0]  &= ~RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] &= ~RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  &= ~RAM_3_2G;
      break;  
      case 2: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  &= ~RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  |= RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;  
      case 3: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;  
      case 4:       
        LCD->RAM[0]  &= ~RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] &= ~RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;
      case 5: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  &= ~RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;
      case 6: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  &= ~RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  |= RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;
      case 7: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] &= ~RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  &= ~RAM_3_2G;
      break;        
      case 8: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  |= RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;
      case 9: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  |= RAM_3_2B;
        LCD->RAM[7]  |= RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;  
      case 10: 
        LCD->RAM[0]  |= RAM_0_2A;
        LCD->RAM[3]  &= ~RAM_3_2B;
        LCD->RAM[7]  &= ~RAM_7_2C;
        LCD->RAM[10] |= RAM_10_2D;
        LCD->RAM[7]  |= RAM_7_2E;        
        LCD->RAM[0]  |= RAM_0_2F;
        LCD->RAM[3]  &= ~RAM_3_2G;
      break;        
      case r2: 
        LCD->RAM[0]  &= ~RAM_0_2A;
        LCD->RAM[3]  &= ~RAM_3_2B;
        LCD->RAM[7]  &= ~RAM_7_2C;
        LCD->RAM[10] &= ~RAM_10_2D;
        LCD->RAM[7]  |= RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;
      break;
      case LINE:
        LCD->RAM[0]  &= ~RAM_0_2A;
        LCD->RAM[3]  &= ~RAM_3_2B;
        LCD->RAM[7]  &= ~RAM_7_2C;
        LCD->RAM[10] &= ~RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  |= RAM_3_2G;  
      break;      
      default: 
        LCD->RAM[0]  &= ~RAM_0_2A;
        LCD->RAM[3]  &= ~RAM_3_2B;
        LCD->RAM[7]  &= ~RAM_7_2C;
        LCD->RAM[10] &= ~RAM_10_2D;
        LCD->RAM[7]  &= ~RAM_7_2E;        
        LCD->RAM[0]  &= ~RAM_0_2F;
        LCD->RAM[3]  &= ~RAM_3_2G;
  } 
}   

/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 3
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos3(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  |= RAM_7_3E;        
        LCD->RAM[0]  |= RAM_0_3F;
        LCD->RAM[4]  &= ~RAM_4_3G;
      break;       
      case 1: 
        LCD->RAM[0]  &= ~RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] &= ~RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;        
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  &= ~RAM_4_3G;
      break;       
      case 2: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  &= ~RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  |= RAM_7_3E;
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break;   
      case 3: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break;   
      case 4: 
        LCD->RAM[0]  &= ~RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] &= ~RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;        
        LCD->RAM[0]  |= RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break;   
      case 5: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  &= ~RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;
        LCD->RAM[0]  |= RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break;         
      case 6: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  &= ~RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  |= RAM_7_3E;
        LCD->RAM[0]  |= RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break;       
      case 7: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] &= ~RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;        
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  &= ~RAM_4_3G;
      break;     
      case 8: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  |= RAM_7_3E;
        LCD->RAM[0]  |= RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break;   
      case 9: 
        LCD->RAM[0]  |= RAM_0_3A;
        LCD->RAM[4]  |= RAM_4_3B;
        LCD->RAM[7]  |= RAM_7_3C;
        LCD->RAM[11] |= RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;
        LCD->RAM[0]  |= RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break; 
      case r3: 
        LCD->RAM[0]  &= ~RAM_0_3A;
        LCD->RAM[4]  &= ~RAM_4_3B;
        LCD->RAM[7]  &= ~RAM_7_3C;
        LCD->RAM[11] &= ~RAM_11_3D;
        LCD->RAM[7]  |= RAM_7_3E;
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G;
      break; 
      case LINE:
        LCD->RAM[0]  &= ~RAM_0_3A;
        LCD->RAM[4]  &= ~RAM_4_3B;
        LCD->RAM[7]  &= ~RAM_7_3C;
        LCD->RAM[11] &= ~RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  |= RAM_4_3G; 
      break;      
      default: 
        LCD->RAM[0]  &= ~RAM_0_3A;
        LCD->RAM[4]  &= ~RAM_4_3B;
        LCD->RAM[7]  &= ~RAM_7_3C;
        LCD->RAM[11] &= ~RAM_11_3D;
        LCD->RAM[7]  &= ~RAM_7_3E;
        LCD->RAM[0]  &= ~RAM_0_3F;
        LCD->RAM[4]  &= ~RAM_4_3G;      
  }
}
   
/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 4
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos4(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  |= RAM_8_4E;
        LCD->RAM[1]  |= RAM_1_4F;
        LCD->RAM[4]  &= ~RAM_4_4G;
      break;   
      case 1: 
        LCD->RAM[1]  &= ~RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] &= ~RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  &= ~RAM_4_4G;
      break;   
      case 2: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  &= ~RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  |= RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break;  
      case 3: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break;  
      case 4: 
        LCD->RAM[1]  &= ~RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] &= ~RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  |= RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break;   
      case 5: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  &= ~RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  |= RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break; 
      case 6: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  &= ~RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  |= RAM_8_4E;
        LCD->RAM[1]  |= RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break;   
      case 7: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] &= ~RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  &= ~RAM_4_4G;
      break; 
      case 8: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  |= RAM_8_4E;
        LCD->RAM[1]  |= RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break;   
      case 9: 
        LCD->RAM[1]  |= RAM_1_4A;
        LCD->RAM[4]  |= RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  |= RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break; 
      case o: 
        LCD->RAM[1]  &= ~RAM_1_4A;
        LCD->RAM[4]  &= ~RAM_4_4B;
        LCD->RAM[8]  |= RAM_8_4C;
        LCD->RAM[11] |= RAM_11_4D;
        LCD->RAM[8]  |= RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break; 
      case LINE:
        LCD->RAM[1]  &= ~RAM_1_4A;
        LCD->RAM[4]  &= ~RAM_4_4B;
        LCD->RAM[8]  &= ~RAM_8_4C;
        LCD->RAM[11] &= ~RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  |= RAM_4_4G;
      break;      
      default: 
        LCD->RAM[1]  &= ~RAM_1_4A;
        LCD->RAM[4]  &= ~RAM_4_4B;
        LCD->RAM[8]  &= ~RAM_8_4C;
        LCD->RAM[11] &= ~RAM_11_4D;
        LCD->RAM[8]  &= ~RAM_8_4E;
        LCD->RAM[1]  &= ~RAM_1_4F;
        LCD->RAM[4]  &= ~RAM_4_4G;
  }    
}    
        
/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 5
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos5(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  |= RAM_8_5E;
        LCD->RAM[1]  |= RAM_1_5F;
        LCD->RAM[4]  &= ~RAM_4_5G;
      break;
      case 1: 
        LCD->RAM[1]  &= ~RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] &= ~RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  &= ~RAM_4_5G;
      break;
      case 2: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  &= ~RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  |= RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break;    
      case 3: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break;    
      case 4: 
        LCD->RAM[1]  &= ~RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] &= ~RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  |= RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break;
      case 5: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  &= ~RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  |= RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break; 
      case 6: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  &= ~RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  |= RAM_8_5E;
        LCD->RAM[1]  |= RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break; 
      case 7: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] &= ~RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  &= ~RAM_4_5G;
      break;
      case 8: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  |= RAM_8_5E;
        LCD->RAM[1]  |= RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break;  
      case 9: 
        LCD->RAM[1]  |= RAM_1_5A;
        LCD->RAM[4]  |= RAM_4_5B;
        LCD->RAM[8]  |= RAM_8_5C;
        LCD->RAM[11] |= RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  |= RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break;  
      case r5: 
        LCD->RAM[1]  &= ~RAM_1_5A;
        LCD->RAM[4]  &= ~RAM_4_5B;
        LCD->RAM[8]  &= ~RAM_8_5C;
        LCD->RAM[11] &= ~RAM_11_5D;
        LCD->RAM[8]  |= RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G;
      break;  
      case LINE:
        LCD->RAM[1]  &= ~RAM_1_5A;
        LCD->RAM[4]  &= ~RAM_4_5B;
        LCD->RAM[8]  &= ~RAM_8_5C;
        LCD->RAM[11] &= ~RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  |= RAM_4_5G; 
      break;      
      default: 
        LCD->RAM[1]  &= ~RAM_1_5A;
        LCD->RAM[4]  &= ~RAM_4_5B;
        LCD->RAM[8]  &= ~RAM_8_5C;
        LCD->RAM[11] &= ~RAM_11_5D;
        LCD->RAM[8]  &= ~RAM_8_5E;
        LCD->RAM[1]  &= ~RAM_1_5F;
        LCD->RAM[4]  &= ~RAM_4_5G;
  }    
}    
      
/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 6
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos6(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  |= RAM_9_6E;
        LCD->RAM[2]  |= RAM_2_6F;
        LCD->RAM[6]  &= ~RAM_6_6G;
      break;          
      case 1: 
        LCD->RAM[2]  &= ~RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] &= ~RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  &= ~RAM_2_6F;
        LCD->RAM[6]  &= ~RAM_6_6G;
      break; 
      case 2: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  &= ~RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  |= RAM_9_6E;
        LCD->RAM[2]  &= ~RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break;     
      case 3: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  &= ~RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break; 
      case 4: 
        LCD->RAM[2]  &= ~RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] &= ~RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  |= RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break;      
      case 5: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  &= ~RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  |= RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break;   
      case 6: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  &= ~RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  |= RAM_9_6E;
        LCD->RAM[2]  |= RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break;  
      case 7: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] &= ~RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  &= ~RAM_2_6F;
        LCD->RAM[6]  &= ~RAM_6_6G;
      break; 
      case 8: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  |= RAM_9_6E;
        LCD->RAM[2]  |= RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break;    
      case 9: 
        LCD->RAM[2]  |= RAM_2_6A;
        LCD->RAM[6]  |= RAM_6_6B;
        LCD->RAM[9]  |= RAM_9_6C;
        LCD->RAM[13] |= RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  |= RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;
      break;  
      case LINE:
        LCD->RAM[2]  &= ~RAM_2_6A;
        LCD->RAM[6]  &= ~RAM_6_6B;
        LCD->RAM[9]  &= ~RAM_9_6C;
        LCD->RAM[13] &= ~RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  &= ~RAM_2_6F;
        LCD->RAM[6]  |= RAM_6_6G;  
      break;      
      default: 
        LCD->RAM[2]  &= ~RAM_2_6A;
        LCD->RAM[6]  &= ~RAM_6_6B;
        LCD->RAM[9]  &= ~RAM_9_6C;
        LCD->RAM[13] &= ~RAM_13_6D;
        LCD->RAM[9]  &= ~RAM_9_6E;
        LCD->RAM[2]  &= ~RAM_2_6F;
        LCD->RAM[6]  &= ~RAM_6_6G;
  }
}

/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 7
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos7(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  |= RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  &= ~RAM_5_7G;     
      break;    
      case 1: 
        LCD->RAM[2]  &= ~RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  &= ~RAM_2_7F;  
        LCD->RAM[5]  &= ~RAM_5_7G;     
      break;    
      case 2: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  &= ~RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  |= RAM_9_7E;
        LCD->RAM[2]  &= ~RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break;  
      case 3: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  &= ~RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break;      
      case 4: 
        LCD->RAM[2]  &= ~RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break; 
      case 5: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  &= ~RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break;
      case 6: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  &= ~RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  |= RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break; 
      case 7: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  &= ~RAM_2_7F;  
        LCD->RAM[5]  &= ~RAM_5_7G;     
      break;  
      case 8: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  |= RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break;
      case 9: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  |= RAM_9_7C;
        LCD->RAM[12] |= RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break;  
      case GRADUS: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  |= RAM_5_7B;
        LCD->RAM[9]  &= ~RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G;     
      break;     
      case YEAR: 
        LCD->RAM[2]  |= RAM_2_7A;
        LCD->RAM[5]  &= ~RAM_5_7B;
        LCD->RAM[9]  &= ~RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  |= RAM_9_7E;
        LCD->RAM[2]  |= RAM_2_7F;  
        LCD->RAM[5]  &= ~RAM_5_7G;     
      break;         
      case LINE:
        LCD->RAM[2]  &= ~RAM_2_7A;
        LCD->RAM[5]  &= ~RAM_5_7B;
        LCD->RAM[9]  &= ~RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  &= ~RAM_2_7F;  
        LCD->RAM[5]  |= RAM_5_7G; 
      break;      
      default: 
        LCD->RAM[2]  &= ~RAM_2_7A;
        LCD->RAM[5]  &= ~RAM_5_7B;
        LCD->RAM[9]  &= ~RAM_9_7C;
        LCD->RAM[12] &= ~RAM_12_7D;      
        LCD->RAM[9]  &= ~RAM_9_7E;
        LCD->RAM[2]  &= ~RAM_2_7F;  
        LCD->RAM[5]  &= ~RAM_5_7G;     
      break;        
  }
}

/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 8
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos8(const uint8_t value)
{
  switch(value)
  {
      case 0:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  |= RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  &= ~RAM_5_8G;
      break;     
      case 1:  
        LCD->RAM[1]  &= ~RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] &= ~RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  &= ~RAM_2_8F;
        LCD->RAM[5]  &= ~RAM_5_8G;  
      break;      
      case 2:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  &= ~RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  |= RAM_9_8E;
        LCD->RAM[2]  &= ~RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;
      break;       
      case 3:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  &= ~RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;
      break; 
      case 4:  
        LCD->RAM[1]  &= ~RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] &= ~RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;  
      break;  
      case 5:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  &= ~RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;
      break;        
      case 6:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  &= ~RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  |= RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;
      break;    
      case 7:  
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] &= ~RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  &= ~RAM_2_8F;
        LCD->RAM[5]  &= ~RAM_5_8G;  
      break;         
      case 8:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  |= RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;
      break;   
      case 9:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  |= RAM_5_8B;
        LCD->RAM[8]  |= RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;
      break;   
      
      case CELSIUS:     
        LCD->RAM[1]  |= RAM_1_8A;
        LCD->RAM[5]  &= ~RAM_5_8B;
        LCD->RAM[8]  &= ~RAM_8_8C;
        LCD->RAM[12] |= RAM_12_8D;
        LCD->RAM[9]  |= RAM_9_8E;
        LCD->RAM[2]  |= RAM_2_8F;
        LCD->RAM[5]  &= ~RAM_5_8G;
      break;  
      
      case LINE:
        LCD->RAM[1]  &= ~RAM_1_8A;
        LCD->RAM[5]  &= ~RAM_5_8B;
        LCD->RAM[8]  &= ~RAM_8_8C;
        LCD->RAM[12] &= ~RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  &= ~RAM_2_8F;
        LCD->RAM[5]  |= RAM_5_8G;  
      break;
      default:     
        LCD->RAM[1]  &= ~RAM_1_8A;
        LCD->RAM[5]  &= ~RAM_5_8B;
        LCD->RAM[8]  &= ~RAM_8_8C;
        LCD->RAM[12] &= ~RAM_12_8D;
        LCD->RAM[9]  &= ~RAM_9_8E;
        LCD->RAM[2]  &= ~RAM_2_8F;
        LCD->RAM[5]  &= ~RAM_5_8G;
  }
}

/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 9
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos9(const uint8_t value)
{
  switch(value)
  {
      case 0:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  &= ~RAM_7_9G;
      break;     
      case 1:  
        LCD->RAM[10] &= ~RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  &= ~RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] &= ~RAM_10_9F;
        LCD->RAM[7]  &= ~RAM_7_9G;  
      break;      
      case 2:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] &= ~RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;       
      case 3:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] &= ~RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break; 
      case 4:  
        LCD->RAM[10] &= ~RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  &= ~RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;  
      break;  
      case 5:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;        
      case 6:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;    
      case 7:  
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  &= ~RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] &= ~RAM_10_9F;
        LCD->RAM[7]  &= ~RAM_7_9G;  
      break;         
      case 8:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;   
      case 9:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  |= RAM_7_9B;
        LCD->RAM[3]  |= RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;   
      case INDUCTANCE:     
        LCD->RAM[10] &= ~RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  &= ~RAM_7_9G;
      break;     
      case CAPACITANCE:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  &= ~RAM_7_9G;
      break;       
      case FREQUENCY:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  &= ~RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;      
      case MINUS:     
        LCD->RAM[10] &= ~RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  &= ~RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] &= ~RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;  
      case EXPORT:     
        LCD->RAM[10] |= RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  |= RAM_0_9D;
        LCD->RAM[3]  |= RAM_3_9E;
        LCD->RAM[10] |= RAM_10_9F;
        LCD->RAM[7]  |= RAM_7_9G;
      break;  
      default:     
        LCD->RAM[10] &= ~RAM_10_9A;
        LCD->RAM[7]  &= ~RAM_7_9B;
        LCD->RAM[3]  &= ~RAM_3_9C;
        LCD->RAM[0]  &= ~RAM_0_9D;
        LCD->RAM[3]  &= ~RAM_3_9E;
        LCD->RAM[10] &= ~RAM_10_9F;
        LCD->RAM[7]  &= ~RAM_7_9G;
  }
}      

/**
  * @brief  ������� ������ ����� 0..9 � �����(255) �� ������� 10
  * @param  Value
  * @retval None
  */
static void lcd_show_number_pos10(const uint8_t value)
{
  switch(value)
  {
      case 0: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  |= RAM_5_10E;
        LCD->RAM[12] |= RAM_12_10F;
        LCD->RAM[8]  &= ~RAM_8_10G;
      break;     
      case 1: 
        LCD->RAM[12] &= ~RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  &= ~RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] &= ~RAM_12_10F;
        LCD->RAM[8]  &= ~RAM_8_10G;    
      break; 
      case 2: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  &= ~RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  |= RAM_5_10E;
        LCD->RAM[12] &= ~RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;
      break;    
      case 3: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] &= ~RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;
      break;
      case 4: 
        LCD->RAM[12] &= ~RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  &= ~RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] |= RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;    
      break; 
      case 5: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  &= ~RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] |= RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;
      break;
      case 6: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  &= ~RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  |= RAM_5_10E;
        LCD->RAM[12] |= RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;
      break;
      case 7: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  &= ~RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] &= ~RAM_12_10F;
        LCD->RAM[8]  &= ~RAM_8_10G;    
      break;       
      case 8: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  |= RAM_5_10E;
        LCD->RAM[12] |= RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;
      break;
      case 9: 
        LCD->RAM[12] |= RAM_12_10A;
        LCD->RAM[8]  |= RAM_8_10B;
        LCD->RAM[5]  |= RAM_5_10C;
        LCD->RAM[1]  |= RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] |= RAM_12_10F;
        LCD->RAM[8]  |= RAM_8_10G;
      break;  
      default:              
        LCD->RAM[12] &= ~RAM_12_10A;
        LCD->RAM[8]  &= ~RAM_8_10B;
        LCD->RAM[5]  &= ~RAM_5_10C;
        LCD->RAM[1]  &= ~RAM_1_10D;
        LCD->RAM[5]  &= ~RAM_5_10E;
        LCD->RAM[12] &= ~RAM_12_10F;
        LCD->RAM[8]  &= ~RAM_8_10G;        
  }
}


/**
  * @brief  ������� ������ ������ ���
  * @param  ���� - ��� �������
  * @retval None
  */
static void lcd_show_symbol(const uint8_t value)
{
  switch(value)
  {
      case T1:        LCD->RAM[0]  |= RAM_0_T1;        break;
      case T2:        LCD->RAM[1]  |= RAM_1_T2;        break;
      case L1:        LCD->RAM[0]  |= RAM_0_L1;        break;
      case L2:        LCD->RAM[4]  |= RAM_4_L2;        break;
      case L3:        LCD->RAM[7]  |= RAM_7_L3;        break;
      case SUM:       LCD->RAM[11] |= RAM_11_SUM;      break;
      case MAX:       LCD->RAM[7]  |= RAM_7_MAX;       break;
      
      case S1:        LCD->RAM[4]  |= RAM_4_S1;        break;
      case S2:        LCD->RAM[0]  |= RAM_0_S2;        break;
      case S3:        LCD->RAM[1]  |= RAM_1_S3;        break;
      case S4:        LCD->RAM[5]  |= RAM_5_S4;        break;
      case S5:        LCD->RAM[8]  |= RAM_8_S5;        break;
      case S6:        LCD->RAM[12] |= RAM_12_S6;       break;
      case S7:        LCD->RAM[12] |= RAM_12_S7;       break;
      case S8:	      LCD->RAM[2]  |= RAM_2_S8;        break;
      case S9:        LCD->RAM[6]  |= RAM_6_S9;        break;

      case S10:       LCD->RAM[9]  |= RAM_9_S10;       break;
      case S11:       LCD->RAM[13] |= RAM_13_S11;      break;
      case S12:       LCD->RAM[13] |= RAM_13_S12;      break;
      case S13:       LCD->RAM[2]  |= RAM_2_S13;       break;
      case S14:       LCD->RAM[5]  |= RAM_5_S14;       break;
      case S15:       LCD->RAM[9]  |= RAM_9_S15;       break;

      case V:         LCD->RAM[2]  |= RAM_2_V;         break;
      case k:         LCD->RAM[6]  |= RAM_6_k;         break;
      case M:         LCD->RAM[9]  |= RAM_9_M;         break;
      case r:         LCD->RAM[12] |= RAM_12_r;        break;
      case h:         LCD->RAM[12] |= RAM_12_h;        break;
      case P1:        LCD->RAM[10] |= RAM_10_P1;       break;
      case P2:        LCD->RAM[11] |= RAM_11_P2;       break;
      case P3:        LCD->RAM[13] |= RAM_13_P3;       break;
      
      case P4:        LCD->RAM[11] |= RAM_11_P4;       break;
      case H:
                      LCD->RAM[10] &= ~RAM_10_9A;
                      LCD->RAM[7]  |= RAM_7_9B;
                      LCD->RAM[3]  |= RAM_3_9C;
                      LCD->RAM[0]  &= ~RAM_0_9D;
                      LCD->RAM[3]  |= RAM_3_9E;
                      LCD->RAM[10] |= RAM_10_9F;
                      LCD->RAM[7]  |= RAM_7_9G;		   break;
					  
  	  case W:		  LCD->RAM[2]  |= RAM_2_V;
	  				  LCD->RAM[2]  |= RAM_2_S13;
					  LCD->RAM[5]  |= RAM_5_S14;	   break;
					  
      case A:		  LCD->RAM[5]  |= RAM_5_S14;
	  				  LCD->RAM[9]  |= RAM_9_S15; 	   break;
					  
      case PO:		  
					  LCD->RAM[10] |= RAM_10_9A;
					  LCD->RAM[7]  |= RAM_7_9B;
					  LCD->RAM[3]  |= RAM_3_9C;
					  LCD->RAM[0]  &= ~RAM_0_9D;
					  LCD->RAM[3]  |= RAM_3_9E;
					  LCD->RAM[10] |= RAM_10_9F;
					  LCD->RAM[7]  &= ~RAM_7_9G; 	   break;
      break;
  }
}

/**
  * @brief  ������� �������� ������ ���
  * @param  ���� - ��� �������
  * @retval None
  */
static void lcd_hide_symbol(const uint8_t value)
{
  switch(value)
  {
      case T1:        LCD->RAM[0]  &= ~RAM_0_T1;       break;
      case T2:        LCD->RAM[1]  &= ~RAM_1_T2;       break;
      case L1:        LCD->RAM[0]  &= ~RAM_0_L1;       break;
      case L2:        LCD->RAM[4]  &= ~RAM_4_L2;       break; 
      case L3:        LCD->RAM[7]  &= ~RAM_7_L3;       break;
      case SUM:       LCD->RAM[11] &= ~RAM_11_SUM;     break;
      case MAX:       LCD->RAM[7]  &= ~RAM_7_MAX;      break;
      case S1:        LCD->RAM[4]  &= ~RAM_4_S1;       break;
      case S2:        LCD->RAM[0]  &= ~RAM_0_S2;       break;
      case S3:        LCD->RAM[1]  &= ~RAM_1_S3;       break;
      case S4:        LCD->RAM[5]  &= ~RAM_5_S4;       break;
      case S5:        LCD->RAM[8]  &= ~RAM_8_S5;       break;
      case S6:        LCD->RAM[12] &= ~RAM_12_S6;      break;
      case S7:        LCD->RAM[12] &= ~RAM_12_S7;      break;
      case S8:	      LCD->RAM[2]  &= ~RAM_2_S8;       break;
      case S9:        LCD->RAM[6]  &= ~RAM_6_S9;       break;
      case S10:       LCD->RAM[9]  &= ~RAM_9_S10;      break;
      case S11:       LCD->RAM[13] &= ~RAM_13_S11;     break;
      case S12:       LCD->RAM[13] &= ~RAM_13_S12;     break;
      case S13:       LCD->RAM[2]  &= ~RAM_2_S13;      break;
      case S14:       LCD->RAM[5]  &= ~RAM_5_S14;      break;
      case S15:       LCD->RAM[9]  &= ~RAM_9_S15;      break;
      case V:         LCD->RAM[2]  &= ~RAM_2_V;        break; 
      case k:         LCD->RAM[6]  &= ~RAM_6_k;        break;
      case M:         LCD->RAM[9]  &= ~RAM_9_M;        break;
      case r:         LCD->RAM[12] &= ~RAM_12_r;       break; 
      case h:         LCD->RAM[12] &= ~RAM_12_h;       break;
      case P1:        LCD->RAM[10] &= ~RAM_10_P1;      break;
      case P2:        LCD->RAM[11] &= ~RAM_11_P2;      break;
      case P3:        LCD->RAM[13] &= ~RAM_13_P3;      break;
      case P4:        LCD->RAM[11] &= ~RAM_11_P4;      break;
      case H:        
                      LCD->RAM[10] &= ~RAM_10_9A;
                      LCD->RAM[7]  &= ~RAM_7_9B;
                      LCD->RAM[3]  &= ~RAM_3_9C;
                      LCD->RAM[0]  &= ~RAM_0_9D;
                      LCD->RAM[3]  &= ~RAM_3_9E;
                      LCD->RAM[10] &= ~RAM_10_9F;
                      LCD->RAM[7]  &= ~RAM_7_9G;
      break;      
  }
}
/**
  * @brief  ����� ������ ������ ���������
  * @param  ���� - ����� ������
  * @note	�������� ������� ���������������� ������ ��� ��� ������� ���������� 8 �������
  * @retval None
  */
static void lcd_num_channel(uint8_t value)
{
  value++;
  switch(value)
  {  
      case 1 : lcd_hide_symbol (S4); lcd_hide_symbol (S5); lcd_hide_symbol (S6); break;
      case 2 : lcd_hide_symbol (S4); lcd_hide_symbol (S5); lcd_show_symbol (S6); break;
      case 3 : lcd_hide_symbol (S4); lcd_show_symbol (S5); lcd_hide_symbol (S6); break;
      case 4 : lcd_hide_symbol (S4); lcd_show_symbol (S5); lcd_show_symbol (S6); break;
      
      case 5 : lcd_show_symbol (S4); lcd_hide_symbol (S5); lcd_hide_symbol (S6); break;
      case 6 : lcd_show_symbol (S4); lcd_hide_symbol (S5); lcd_show_symbol (S6); break;
      case 7 : lcd_show_symbol (S4); lcd_show_symbol (S5); lcd_hide_symbol (S6); break;
      case 8 : lcd_show_symbol (S4); lcd_show_symbol (S5); lcd_show_symbol (S6); break;
      
      default: lcd_hide_symbol (S4); lcd_hide_symbol (S5); lcd_hide_symbol (S6); break;
  }
}


/**
  * @brief  ����� ����� � �������� �������
  * @param  ���� - �������� - �����
  * @param  �������
  * @retval None
  */
void lcd_show_val_pos(const u8 value, u8 position)
{
  position--;
  switch(position)
  {
  	  default:
      case 0 : lcd_show_number_pos1 (value); break;
      case 1 : lcd_show_number_pos2 (value); break;
      case 2 : lcd_show_number_pos3 (value); break;
      case 3 : lcd_show_number_pos4 (value); break;
      case 4 : lcd_show_number_pos5 (value); break;
        
      case 5 : lcd_show_number_pos6 (value); break;
      case 6 : lcd_show_number_pos7 (value); break;
      case 7 : lcd_show_number_pos8 (value); break;
      case 8 : lcd_show_number_pos9 (value); break;
      case 9 : lcd_show_number_pos10(value); break;
  }
}

/**
  * @brief  �������� ���� � �������� ������
  * @param  None
  * @retval None
  */
static void lcd_show_zeroes()
{
    for (u8 i = 0; i < 8; i++)
    {
        lcd_show_val_pos(0, (i+1));
    }
}

/**
  * @brief  ������� ������, ��������� ��������
  * @param  None
  * @retval None
  */
#define CLEAR_NUMBER 20
static void lcd_clear_data()
{
    for (u8 i = 0; i < 8; i++)
    {
        lcd_show_val_pos(CLEAR_NUMBER, (i+1));
    }
	lcd_hide_symbol(POINT);
}


/**
  * @brief  ����� 32 ���������� ����� �� ���
  * @param  ���� - 32 ��������� ����� �� ����� � ���� ���������� �� ���� ����� ������
  * @param  ��������� ������� ������� ������
  * @param  ���������� ����� ����� ������ �����������
  * @retval None
  */
static void lcd_show_long(s32 data, const bool fShowZeroes, const bool fShowMinusObligatorily)
{
	u8 i = 0;
	u8 pos = 8;
	
	lcd_clear_data();

	// ** ���� ������ ����� ���, ������� ���� � �������
	if (0L == data)
	{
		if (true == fShowZeroes)    {
			lcd_show_zeroes();
		}
		// ** ����� ���� ����� 
	  	for (u8 j = 8; j >= 6; j--) {	  
	  		lcd_show_val_pos(0,j);
		}
		// ** ����������� ���������� �����
		if ((fShowMinusObligatorily) && (false == fShowZeroes))
		{
			lcd_show_val_pos(LINE,5);
		}
		return;
	}
	if (data > 0)
	// ** ����� �������������
	{
		if (true == fShowZeroes)    {
			lcd_show_zeroes();
		}
		// ** ����� �� ������ ��� �������� ��������� �����
		// ** ���������� ���������� ��� ��������
		while(data)
		{
			lcd_show_val_pos((u8)(data % 10L),pos--);		
			data /= 10L, i++;
		}
		
		// ** ���� ���������� �������� ������ 3 
		// ** ������� ���� �� �������� 6 ��� (6 � 7) ��� (6 � 7 � 8)
		while (i < 3)
		{
			pos = 8 - i;
			lcd_show_val_pos(0,pos);
			i++; 
		}
		// ** ����������� ���������� �����
		if ((fShowMinusObligatorily) && (false == fShowZeroes))
		{
		  	pos = 8 - i;
			lcd_show_val_pos(LINE,pos);
			i++; 
		}
	}
	else
	// ** ����� �������������
	{
	  	data = (s32)fabs((float)data);
		// ** ����� �� ������ ��� �������� ��������� �����
		// ** ���������� ���������� ��� ��������
		while(data)
		{
			lcd_show_val_pos((u8)(data % 10L),pos--);		
	//		lcd_show_val_pos(LINE,pos - 1);
			data /= 10L, i++;
		}
		
		lcd_show_val_pos(LINE,pos);
		
		// ** ���� ���������� �������� ������ 3 
		// ** ������� ���� �� �������� 6 ��� (6 � 7) ��� (6 � 7 � 8)
		while (i < 3)
		{
			lcd_show_val_pos(0,8 - i);
			lcd_show_val_pos(LINE,8 - i - 1);
			i++; 
		}
		
	}
}


/**
  * @brief  ����� 32 ���������� ����� �� ���
  * @param  ���� - 32 ��������� ����� �� ����� � ���� ���������� �� ���� ����� ������
  * @param  ��������� ������� ������� ������
  * @param  ���������� ����� ����� ������ �����������
  * @retval None
  */
static void lcd_show_bcd(s32 data, const bool fShowZeroes, const bool fShowMinusObligatorily)
{
	u8 i = 0;
	u8 pos = 8;
	
	lcd_clear_data();
	
	// ** ���� ������ ����� ���, ������� ���� � �������
	if (0L == data)
	{
		if (true == fShowZeroes)    {
			lcd_show_zeroes();
		}
		// ** ����� ���� ����� 
	  	for (u8 j = 8; j >= 6; j--) {	  
	  		lcd_show_val_pos(0,j);
		}
		// ** ����������� ���������� �����
		if ((fShowMinusObligatorily) && (false == fShowZeroes))
		{
			lcd_show_val_pos(LINE,5);
		}
		return;
	}
	
	if (true == fShowZeroes)    {
		lcd_show_zeroes();
	}
	// ** ����� �� ������ ��� �������� ��������� �����
	// ** ���������� ���������� ��� ��������
	while(data)
	{
		lcd_show_val_pos((u8)(data & 0xF),pos--);		
		data >>= 4, i++;
	}
	
	// ** ���� ���������� �������� ������ 3 
	// ** ������� ���� �� �������� 6 ��� (6 � 7) ��� (6 � 7 � 8)
	while (i < 3)
	{
		pos = 8 - i;
		lcd_show_val_pos(0,pos);
		i++; 
	}
}

/**
  * @brief  ����� 32 ���������� ����� �� ���
  * @param  ���� - float ����� ��� ������ �� ���
  * @param  ��������� ������� ������� ������
  * @param  ���������� ����� ����� ������ �����������
  * @retval None
  */
static void lcd_show_float(float dataf, const bool fShowZeroes, const bool fShowMinusObligatorily)
{
  	// ** �������������� float � long
	lcd_show_long((s32)(dataf * (float)100), fShowZeroes, fShowMinusObligatorily);
	
}

// ** �������� ������ ������ ����
static void lcd_show_lines()
{
	lcd_show_val_pos( LINE, 1);
	lcd_show_val_pos( LINE, 2);
	lcd_show_val_pos( LINE, 3);
	lcd_show_val_pos( LINE, 4);
	lcd_show_val_pos( LINE, 5);
	lcd_show_val_pos( LINE, 6);
	lcd_show_val_pos( LINE, 7);
	lcd_show_val_pos( LINE, 8);
}

static void lcd_text(const bool showFlag, u16 num, ...)
{
  	va_list ptr;
  	va_start( ptr, num);
	
	while(num--)
  	{
	  	if(showFlag)
		{
			lcd_show_symbol(va_arg(ptr, u16));
		}
		else
		{
		  	lcd_hide_symbol(va_arg(ptr, u16));
		}
	}
	va_end(ptr);
}

// ** ����� �� ��� ������� 
static void lcd_show_time(const RTC_TimeTypeDef* TimeStructure)
{
	lcd_show_val_pos(TimeStructure->RTC_Hours   / 10,3);
	lcd_show_val_pos(TimeStructure->RTC_Hours   % 10,4);
	lcd_show_val_pos(TimeStructure->RTC_Minutes / 10,5);
	lcd_show_val_pos(TimeStructure->RTC_Minutes % 10,6);
	lcd_show_val_pos(TimeStructure->RTC_Seconds / 10,7);
	lcd_show_val_pos(TimeStructure->RTC_Seconds % 10,8);	
}

// ** ����� �� ��� ����
static void lcd_show_date(const RTC_DateTypeDef* DateStructure)
{
	lcd_show_val_pos(DateStructure->RTC_Date / 10,1);
	lcd_show_val_pos(DateStructure->RTC_Date % 10,2);                                        
	lcd_show_val_pos(DateStructure->RTC_Month / 10,3);
	lcd_show_val_pos(DateStructure->RTC_Month % 10,4);
	lcd_show_val_pos(DateStructure->RTC_Year / 10,5);
	lcd_show_val_pos(DateStructure->RTC_Year % 10,6);
}


// ** ������� � ��������������� LCD
/*
static void lcd_deinit(void)
{
  uint8_t counter = 0;

  LCD->CR1 = LCD_CR1_RESET_VALUE;
  LCD->CR2 = LCD_CR2_RESET_VALUE;
  LCD->CR3 = LCD_CR3_RESET_VALUE;
  LCD->FRQ = LCD_FRQ_RESET_VALUE;

  for (counter = 0;counter < 0x05; counter++)
  {
    LCD->PM[counter] = LCD_PM_RESET_VALUE;
  }

  for (counter = 0;counter < 0x16; counter++)
  {
    LCD->RAM[counter] =  LCD_RAM_RESET_VALUE;
  }

  LCD->CR4 = LCD_CR4_RESET_VALUE;
}

*/
static void lcd_show_left_int(u32 data)
{
	lcd_clear_data();

	// ** ���� ������ ����� ���, ������� ���� � �������
	u8 i = 0;
	u8 pos = 5;
	
	if (0 == data)
	{
		// ** ����� ���� ����� 
	  	for (u8 j = 5; j >= 2; j--) {	  
	  		lcd_show_val_pos(0,j);
		}
	}
	else
	{
		while(data)
		{
			lcd_show_val_pos((u8)(data % 10L),pos--);		
			data /= 10L, i++;
		}
		while (i < 4)
		{
			pos = 5 - i;
			lcd_show_val_pos(0,pos);
			i++; 
		}
	}
}




const struct LCD_driver lcd = 
{
	.init   	 		= lcd_init,
	.num_channel 		= lcd_num_channel,
	.show_left_int 		= lcd_show_left_int,
	.show_long 	 		= lcd_show_long,
	.show_float	 		= lcd_show_float,
	.show_bcd 	 		= lcd_show_bcd,
	.show_lines  		= lcd_show_lines,
	.all				= lcd_all,
	.clear				= lcd_clear,
//	.show_symbol 		= lcd_show_symbol,
//	.hide_symbol 		= lcd_hide_symbol,
	.show_val_pos		= lcd_show_val_pos,
	.text				= lcd_text,
	.show_date 			= lcd_show_date,
	.show_time 			= lcd_show_time,
	.clear_data			= lcd_clear_data,
};



/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/


