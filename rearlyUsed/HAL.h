/**
  ******************************************************************************
  * @file    HAL.h
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   This file contains all the pin definitions
  */

#pragma once

/* Private macro -------------------------------------------------------------*/

// ** SPI ADE

#define SPI_MISO_PIN                  GPIO_Pin_2 // PA2
#define SPI_MOSI_PIN                  GPIO_Pin_3 // PA3
#define SPI_SCK_PIN                   GPIO_Pin_6 // PC6
#define SPI_CS_PIN                    GPIO_Pin_5 // PC5

#define SPI_MISO_GPIO		          GPIOA // PA2
#define SPI_MOSI_GPIO		          GPIOA // PA3
#define SPI_SCK_GPIO	              GPIOC // PC6
#define SPI_CS_GPIO              	  GPIOC // PC5

#define CS_HIGH                       do {SPI_CS_GPIO->ODR |=  SPI_CS_PIN; } while(0)
#define CS_LOW                        do {SPI_CS_GPIO->ODR &= ~SPI_CS_PIN; } while(0)

#define ADE_SPI					  	SPI1


// ** ��������������� ���� ADE

#define ADE_RESET_PIN               GPIO_Pin_6 // PE6
#define ADE_RESET_GPIO		        GPIOE

#define ADE_ISR_PIN                 GPIO_Pin_7 // PE7
#define ADE_ISR_GPIO		        GPIOE
#define ADE_EXTI_Pin_ISR	        EXTI_Pin_7

#define ADE_TMA_PIN                 GPIO_Pin_0 // PC0
#define ADE_TMA_GPIO		        GPIOC

#define ADE_ZX_PIN                  GPIO_Pin_1 // PC1
#define ADE_ZX_GPIO		          	GPIOC

#define IS_ADE_ZX_HIGH	 			(ADE_ZX_PIN  == (ADE_ZX_GPIO->IDR & ADE_ZX_PIN))
#define IS_ADE_ZX_LOW 				(!IS_ADE_ZX_HIGH)


// ** ���������

#define CALIBRATION_PIN           	GPIO_Pin_0
#define CALIBRATION_GPIO 			GPIOF

// ** USART ��� ����������

#define USART_TX_PIN           		GPIO_Pin_3
#define USART_RX_PIN           		GPIO_Pin_2

#define USART_TX_GPIO 				GPIOC
#define USART_RX_GPIO 				GPIOC

#define USART_CALIBR 				USART1
#define USART_BAUDRATE 				(u32)9600
#define USART_RX_TIMEOUT			5

#define IS_USART_TC	 				(USART_SR_TC == (USART1->SR & USART_SR_TC))


/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/