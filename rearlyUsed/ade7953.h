
/**
  ******************************************************************************
  * @file    ade7953.h
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    18-June-2015
  * @brief   Module contain fuctions for working with ADE7953 chip
  ******************************************************************************  
  */
#pragma once


#define ADE_REG_DISNOLOAD	0x01
#define ADE_REG_LCYCMODE	0x004
#define ADE_REG_PGA_IA		0x008


#define ADE_REG_PERIOD 		0x10E
#define ADE_REG_ZXTOUT 		0x100
#define ADE_REG_CONFIG 		0x102
#define ADE_REG_CF1DEN 		0x103
#define ADE_REG_CF2DEN 		0x104
#define ADE_REG_CFMODE		0x107
#define ADE_REG_PHCALA		0x108

#define ADE_REG_ACCMODE		0x201
#define ADE_REG_AP_NOLOAD	0x203	
#define ADE_REG_VAR_NOLOAD	0x204
#define ADE_REG_VA_NOLOAD	0x205

#define ADE_REG_IRMSA		0x21A
#define ADE_REG_VRMS		0x21C
#define ADE_REG_AENERGYA	0x21E
#define ADE_REG_RENERGYA	0x220
#define ADE_REG_APENERGYA	0x222

#define ADE_REG_IRQENA		0x22C
#define ADE_REG_RSTIRQSTATA 0x22E

#define ADE_REG_AIGAIN 		0x280
#define ADE_REG_VGAIN 		0x281
#define ADE_REG_AWGAIN 		0x282
#define ADE_REG_AVARGAIN	0x283
#define ADE_REG_AVAGAIN		0x284
#define ADE_REG_AIRMSOS		0x286

#define ADE_REG_VRMSOS		0x288
#define ADE_REG_AWATTOS		0x289
#define ADE_REG_AVAROS		0x28A
#define ADE_REG_AVAOS		0x28B


#define ADE_START() GPIO_SetBits(ADE_RESET_GPIO, ADE_RESET_PIN)
#define ADE_STOP()  GPIO_ResetBits(ADE_RESET_GPIO, ADE_RESET_PIN)


#define IRQSTATA_ADDR   (u16)0x22E
#define IRQSTATA_RESET  (uint32_t )0x00100000
#define IRQSTATA_ZXV    (uint32_t )0x00008000
#define IRQSTATA_ZXTO   (uint32_t )0x00004000
/*489.24:  

	��� ������������� �������� ���� ��� ���������� - ��� ����� 9032007d
	��� ������������� �������� �������� 		   - ��� ����� 4862401d
	  
	CFxDEN = 4227, ��� 1 LSB �������� ����������� ������� = 0,01 Wh 

	full scale 
	Ifs = (0,5 / (Rshunt *sqrt(2) * 16))	= 0,5 V / (200e-6 Ohm *sqrt(2)*16)		=  110.485 [A]
	Vfs = (0,5*(R12+(4*R4)))/(R12*sqrt(2)) 	= (0,5*(1kOm+4*270kOm))/(1kOm*sqrt(2)) 	=  382,191 [V]

	���� ��� ����������� ���������� � ����
	IRMSref = round((In/Ifs)*9032007d) =   round((  5/ 110.485)* 9032007d) 	= 408743.585
	VRMSref = round((Vn/Vfs)*9032007d) =   round((230/ 382,191)* 9032007d) 	= 5435401.697

	������� ������ ���� �� 1 �����
	IRMS_SCALE  = IRMSref/In = 408743.585/5 		= 81748.717
	
	������� ������ ���� �� 1 �����
	VRMS_SCALE  = VRMSref/Vn = 5435401.697/230 		= 23632.181

	������� ������ ���� �� 1 ����
	POWER_SCALE = 1000*PMAX/(Vfs*Ifs) = (1000*4862401)/(382,191*110.485) = 115150.81

	��� �������� ����������� ����������� �������� 
	NOLOAD = round((Vn*Inoload*PMAX)/(Vfs*Ifs)) = round((230*0,015*4862401)/(382,191*110.485)) = 397.27


	����������� ���										5		 [�]
	����������� ����������  							230 	 [V]
	���, ��� ������� �� ����� ADE 0,5 ������			110.485  [�]
	����������, ��� ������� �� ����� ADE 0,5 ������		382.191  [V]
	���������� ��������	���� �� ����� 					16 

*/
/* ���������� Ade7953 �� ��������� -------------------------------------*/
#define AWATTOS_DEFAULT                 ((uint32_t)0x000000)
#define AVAROS_DEFAULT                  ((uint32_t)0x000000)
#define AVAOS_DEFAULT                   ((uint32_t)0x000000)

#define AWGAIN_DEFAULT                  ((uint32_t)0x400000)
#define AVARGAIN_DEFAULT                ((uint32_t)0x400000)
#define AVAGAIN_DEFAULT                 ((uint32_t)0x400000)
#define AIRMSOS_DEFAULT                 ((uint32_t)0x000000)
#define VRMSOS_DEFAULT                  ((uint32_t)0x000000)
#define PHCALA_DEFAULT                  ((uint16_t)0x000000)
#define AIGAIN_DEFAULT			     	((uint32_t)0x400000)
#define VGAIN_DEFAULT      				((uint32_t)0x400000)

#define AIRMSSCALE_EXTERNAL_DEFAULT     ((uint32_t)81748)
#define VRMSSCALE_EXTERNAL_DEFAULT      ((uint32_t)23632)
#define PWRSCALE_EXTERNAL_DEFAULT       ((uint32_t)115150)



//#define BIGAIN_DEFAULT                  ((uint32_t)0x400000)
//#define BIRMSOS_DEFAULT                 ((uint32_t)0x000000)


/* Exported constants --------------------------------------------------------*/
#define ADE_CFxDEN                     4427

/* ��� ������� ������� 0.5� : ��� - 110.485 �, ���������� - 382,191 �.
 ��������� ��� - 15 ��, ����������� ���������� - 230 �.
 X_NOLOAD = 65536 - Y/1.4
 Y = (110.485 * 382,191)/(0.012 * 230) = 15299  // 12239
 X_NOLOAD = 65536 - 12239/1.4 = 54607 */ //56793
#define ADE_NOLOAD_VAL                     54607

/* The period measurement has a resolution of 4.67 mks/LSB (~214 kHz clock = 
 48 MHz / (14*16)), ADE_LSB_PERIOD = 214285.71428571428571428571428571 Hz * 50 
(��� 50 ������� �������)*/
#define ADE_LSB_PERIOD                 10714285.714285714285714285714286f

/* Exported variables ---------------------------------------------------------*/


/**
\brief ������� ADE.
*/
struct ADE_driver
{
	void (*initPins)();
	void (*resetSetSPI)();
	u32  (*read)(u16);
	void (*write)(uint16_t Address, uint32_t Value);
};
extern const struct ADE_driver ade;


void Calculate_TMI_AEnergy_Plus();
static void ADE_InitPins();
/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/


