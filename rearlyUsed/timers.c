/**
  ******************************************************************************
  * @file    timers.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Module contain fuctions for working with system timer
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include <string.h>

/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static u32 timers[MAX_TIMERS];

/* Private functions ---------------------------------------------------------*/


/**
  * @brief  ������������� ���������� ������� 
  * @note   ��� 16 ��� : ������ 0x7D, ������������ 128 - 1 �� ��������� ������
  * @retval None
  */
static void osTimInit()
{
  memset(timers, 0, MAX_TIMERS);
  
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
        
    TIM3_PrescalerConfig(TIM3_Prescaler_128, TIM3_PSCReloadMode_Immediate);
    TIM3_TimeBaseInit(TIM3_Prescaler_128, TIM3_CounterMode_Up, 0x7D);
    
    TIM3_UpdateRequestConfig(TIM3_UpdateSource_Regular);
    TIM3_ITConfig(TIM3_IT_Update, ENABLE);
    TIM3_ClearFlag(TIM3_FLAG_Update);
	TIM3_Cmd(ENABLE);
}

/**
  * @brief  ����� ���������� �������
  * @param  ����� �������
  * @retval None
  */
static void resetTimer(const u8 timer)
{
  	timers[timer] = (u8)0;
}

/**
  * @brief  �������� �������� ���������� �������
  * @param  ����� �������
  * @retval None
  */
static u32 getTimer(const u8 timer)
{
  	return timers[timer];
}

/**
  * @brief  ���������� ���������� ���������� �������
  * @param  ����� �������
  * @retval None
  */
INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_IRQHandler,21)
{
	TIM3_ClearITPendingBit(TIM3_IT_Update);

  for(u8 i = 0; i < MAX_TIMERS; i++)
	{
	  	timers[i]++;
	}
}

/**
  * @brief  ��������� ������� TIM.
*/
const struct Timer_driver tim = 
{
	.get 	= getTimer,
	.reset  = resetTimer,
	.init   = osTimInit,
};

/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/