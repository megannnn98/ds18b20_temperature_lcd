/**
  ******************************************************************************
  * @file    eeCoding.h
  * @author  Karatanov M.N.
  * @version 0.1
  * @date    18-June-2015
  * @brief   eeprom functions header
  ******************************************************************************
*/

#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_flash.h"


typedef enum {
	STATUS_ERROR 		= 0, 
	STATUS_OK 			= 1, 
	STATUS_WAS_ERROR 	= 2
} MEM_STATUS_t;


/* Private define ------------------------------------------------------------*/
#define I_EE_NUMDDM    		  	  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*0))
#define I_EE_CNT_TELEMETRY_GLOBAL ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*1))
#define I_EE_AWGain 			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*2))
#define I_EE_AVarGain 			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*3))

#define I_EE_AVAGain 			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*4))
#define I_EE_AWattOs 			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*5))
#define I_EE_AVarOs  			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*6))
#define I_EE_AVAOs   			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*7))
#define I_EE_AIrmsOs 			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*8))

#define I_EE_PhCalA			 	  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*9))
#define I_EE_VrmsOs 			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*10))
#define I_EE_AIGAIN  			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*11))
#define I_EE_VGAIN  			  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*12))
#define I_EE_TIME_OF_WORK		  ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + (6*13))

#if (0x00001000 + 6*14) > (0x00001000 + 128)

#warning "NO PLACE IN EEPROM"

#endif

#define II_EE_NUMDDM    		   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*0))
#define II_EE_CNT_TELEMETRY_GLOBAL ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*1))
#define II_EE_AWGain 			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*2))
#define II_EE_AVarGain 			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*3))

#define II_EE_AVAGain 			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*4))
#define II_EE_AWattOs 			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*5))
#define II_EE_AVarOs  			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*6))
#define II_EE_AVAOs   			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*7))
#define II_EE_AIrmsOs 			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*8))

#define II_EE_PhCalA			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*9))
#define II_EE_VrmsOs 			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*10))
#define II_EE_AIGAIN  			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*11))
#define II_EE_VGAIN  			   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*12))
#define II_EE_TIME_OF_WORK		   ((u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS + FLASH_BLOCK_SIZE + (6*13))







							 

#define MEM_readByte 		FLASH_ReadByte
#define taskENTER_CRITICAL();				
#define taskEXIT_CRITICAL();
#define MEM_BLOCK_SIZE		FLASH_BLOCK_SIZE
							 

/*!
    \brief Class  memory

    \details Class contain all external functions for work with eeprom 
*/
struct Mem_driver
{
	MEM_STATUS_t(*writeDataTwoCopyes)(uint16_t adress, void* dataPtr);
	MEM_STATUS_t(*readDataTwoCopyes)(uint16_t adress, void* dataPtr, uint32_t defaultVal);
};

extern const struct Mem_driver mem;

/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/