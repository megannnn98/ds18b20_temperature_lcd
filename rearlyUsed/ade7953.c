/**
  ******************************************************************************
  * @file    ade7953.c 
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   chip ade7953 via SPI
  *******************************************************************************/


/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "ade7953.h"
#include "delay.h"

/* Private define ------------------------------------------------------------*/
#define ADE_READ		0x80
#define ADE_WRITE		0x00

/* Private functions ---------------------------------------------------------*/

/**
  * @brief Send and recieve one byte
  * @param byte for send
  * @retval recieved byte
**/
static u8 ADE_SPI_exchange(u8 data)
{
	ADE_SPI->DR = data;
	while(SPI_SR_RXNE != (ADE_SPI->SR & SPI_SR_RXNE));
	return ((uint8_t)ADE_SPI->DR);
}

/**
  * @brief ��������� 1 ���� �� �������� SPI ����������.
  * @param ����� �������� � ADE.
**/
static uint32_t ADE_Read_Register(uint16_t Address)
{
	static uint32_t value = 0x00000000uL;
	value = 0x00000000uL;

	
	CS_LOW;

		ADE_SPI_exchange(BYTE_1(Address));
		ADE_SPI_exchange(BYTE_0(Address));
		ADE_SPI_exchange(ADE_READ);
		

		// � ����������� �� ����������� ������ ������ ������ ���������� ����������
		switch (BYTE_1(Address))
		{
			case 0:
			
				value = ADE_SPI_exchange(0) <<  0;
				
			break;			
			case 1:
			
				value  = ((u32)ADE_SPI_exchange(0)) <<  8;
				value |= ((u32)ADE_SPI_exchange(0)) <<  0;
				
			break;
			
			default:
			  
			  	value  = ((u32)ADE_SPI_exchange(0)) << 16;
				value |= ((u32)ADE_SPI_exchange(0)) <<  8;
				value |= ((u32)ADE_SPI_exchange(0)) <<  0;
		}
		
	CS_HIGH;
	
	
	return  (value & 0x00FFFFFF); 
}

	
/**
  * @brief ���������� 1 ���� �������� SPI ����������.
  * @param ����� �������� � ADE.
  * @param Value �������� 
**/
static void ADE_Write_Register(uint16_t Address, uint32_t Value)
{
	
	CS_LOW;
	
		ADE_SPI_exchange(BYTE_1(Address));
		ADE_SPI_exchange(BYTE_0(Address));
		ADE_SPI_exchange(ADE_WRITE);
		
		switch ((Address >> 8) & 0xFF)
		{
			case 0:
			
				ADE_SPI_exchange(Value);
				
			break;
			case 1:
			
				ADE_SPI_exchange(BYTE_1(Value));
				ADE_SPI_exchange(BYTE_0(Value));
				
			break;
			default:

				ADE_SPI_exchange(BYTE_2(Value));
				ADE_SPI_exchange(BYTE_1(Value));
				ADE_SPI_exchange(BYTE_0(Value));
		}
		
	CS_HIGH;
	
}


/**
  * @brief  Write start sequence for ADE7953
  * @param  None
  * @retval None
  *//*
static void ADE_StartConfig()
{
	ADE_Write_Register(0xFE, 0xAD);
	ADE_Write_Register(0x120, 0x0030);
}
*/



/**
  * @brief  Set SPI as primary interface
  * @param  None
  * @retval None
  */
static void ADE_ResetSetSPI()
{
  	volatile u32 tmp = 0;

	ADE_STOP();
	delays.us10(2);
	ADE_START();
	
	// ** ���������� ���������� - ������, ����� ������.
	// ** ����� ������ ����� ������� CS � ���� ������� 1.2 ���
	tmp = ade.read(ADE_REG_CONFIG) & 0xFFFE;
	
	
	CS_LOW;
	
		ADE_SPI_exchange(BYTE_1(ADE_REG_CONFIG));
		ADE_SPI_exchange(BYTE_0(ADE_REG_CONFIG));
		ADE_SPI_exchange(ADE_WRITE);
		
		ADE_SPI_exchange(BYTE_1(tmp));
		ADE_SPI_exchange(BYTE_0(tmp));
		
		delays.us10(1);		
		
	CS_HIGH;
	
}

/**
  * @brief  Set SPI as primary interface
  * @param  None
  * @retval None
  */
static void ADE_InitPins()
{

	GPIO_Init(SPI_MISO_GPIO, 	SPI_MISO_PIN, GPIO_Mode_In_PU_No_IT);
	GPIO_Init(SPI_MOSI_GPIO, 	SPI_MOSI_PIN, GPIO_Mode_Out_PP_Low_Fast);
	GPIO_Init(SPI_SCK_GPIO, 	SPI_SCK_PIN,  GPIO_Mode_Out_PP_Low_Fast);
	GPIO_Init(SPI_CS_GPIO, 		SPI_CS_PIN,   GPIO_Mode_Out_PP_Low_Fast);
	
	// ** ��������� SPI
    SYSCFG_REMAPPinConfig(REMAP_Pin_SPI1Full, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);    
    SPI_Init(ADE_SPI, SPI_FirstBit_MSB, 
					  SPI_BaudRatePrescaler_8, 
					  SPI_Mode_Master, 
					  SPI_CPOL_Low,
					  SPI_CPHA_1Edge, 
					  SPI_Direction_2Lines_FullDuplex, 
					  SPI_NSS_Soft, 0x00);
	ADE_SPI->CR1 |= SPI_CR1_SPE;
	// ** ����� ADE
	GPIO_Init(ADE_RESET_GPIO, ADE_RESET_PIN, GPIO_Mode_Out_PP_Low_Slow);
	
	ADE_RESET_GPIO->ODR &= ~ADE_RESET_PIN;
	delays.us10(2);
	ADE_RESET_GPIO->ODR |= ADE_RESET_PIN;
}


/**
  * @brief  ��������� ������� ADE.
*/
const struct ADE_driver ade = 
{
  	.initPins		= ADE_InitPins,
	.resetSetSPI 	= ADE_ResetSetSPI,
	.read   		= ADE_Read_Register,
	.write  		= ADE_Write_Register,
};


/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/



