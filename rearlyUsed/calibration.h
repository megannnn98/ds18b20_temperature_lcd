
#pragma once


/**
  ******************************************************************************
  * @file    calcCRC.h
  * @author  Karatanov M.N.
  * @version 0.1
  * @date    18-June-2015
  * @brief   calibration defines header
  ******************************************************************************
  * @note
  * A.
  * ������� ���� (A�D)
  * 	B.
  * 	��� �������
  * 		C.
  * 		������(�������������� ����)
  * 			D.
  * 			CRC
  * 			(A�C)
*/



// ** ���� ������ ��������� 
#define	SERIAL_ERR_FUNC	1
#define	SERIAL_ERR_LEN	2
#define	SERIAL_ERR_CRC	3
#define	SERIAL_ERR_DAT	4
#define	SERIAL_ERR_MEM	5

// 0 - ������
// ** ������� ��������� 
#define	SERIAL_CMD_TYPE_0       0
//#define	SERIAL_CMD_TEMPERATURE  1
//#define	SERIAL_CMD_RTC_0        2
//#define	SERIAL_CMD_RTC_1		3
#define	SERIAL_CMD_NUMBER_0		4
#define	SERIAL_CMD_NUMBER_1		5
//#define	SERIAL_CMD_PASSWORD_0	6?
//#define	SERIAL_CMD_PASSWORD_1	7?



#define	SERIAL_CMD_CALIBR_0		8/*
   0 - AWGain
   1 - AVarGain
   2 - AVAGain
   3 - AWattOs
   4 - AVarOs
   5 - AVAOs
   6 - AIrmsOs
   7 - VrmsOs
   8 - PhCalA
//   9 - AIrmsScaleExternal
//  10 - VrmsScaleExternal
//  11 - ADE_CFxDEN
//  12 - BIGain
//  13 - BIrmsOs
  0E - AIGAIN
  0F - VGAIN
*/
#define	SERIAL_CMD_CALIBR_1		9/*
   0 - AWGain
   1 - AVarGain
   2 - AVAGain
   3 - AWattOs
   4 - AVarOs
   5 - AVAOs
   6 - AIrmsOs
   7 - VrmsOs
   8 - PhCalA
//   9 - AIrmsScaleExternal
//  10 - VrmsScaleExternal
//  12 - BIGain
//  13 - BIrmsOs
  0E - AIGAIN
  0F - VGAIN
*/
#define	SERIAL_CMD_ADE_RESET	0x0A //*
#define	SERIAL_CMD_ADE_DEFAULT	0x0B //*
#define	SERIAL_CMD_APOWER		0x0C //*
#define	SERIAL_CMD_RPOWER		0x0D //*
#define	SERIAL_CMD_FPOWER		0x0E //*
#define	SERIAL_CMD_IRMS 		0x0F //*
#define	SERIAL_CMD_VRMS 		0x10 //*
#define	SERIAL_CMD_FREQ 		0x11 //*
// #define	SERIAL_CMD_NEUTRAL 		 18
// #define	SERIAL_CMD_DATE_0        19
// #define	SERIAL_CMD_DATE_1		 20
#define	GET_TIME_OF_WORK		0x14
#define	GET_PULSES				0x15

/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/