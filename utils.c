
/**
  ******************************************************************************
  * @file    utils.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Module contains common functions
  ******************************************************************************  
  */
/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "math.h"
#include "ade7953.h"
#include "eeCoding.h"
#include "calcCRC.h"

	
// ** ��������� �������
void supplyEn()
{
	GPIO_Init(POWER_GPIO, POWER_PIN, GPIO_Mode_Out_PP_High_Fast);	
}


void btn_init()
{
	GPIO_Init(BTN_GPIO, BTN_PIN, GPIO_Mode_In_FL_No_IT);
}


/************************END OF FILE****/