
/**
  ******************************************************************************
  * @file    thread_ade.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Perform ADE Thread
  ******************************************************************************  
  */

/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "delay.h"
#include "ade7953.h"
#include "eeCoding.h"
/*489.24:  

	��� ������������� �������� ���� ��� ���������� - ��� ����� 9032007d
	��� ������������� �������� �������� 		   - ��� ����� 4862401d
	  
	CFxDEN = 4227, ��� 1 LSB �������� ����������� ������� = 0,01 Wh 

	full scale 
	Ifs = (0,5 / (Rshunt *sqrt(2) * 16))	= 0,5 V / (200e-6 Ohm *sqrt(2)*16)		=  110.485 [A]
	Vfs = (0,5*(R12+(4*R4)))/(R12*sqrt(2)) 	= (0,5*(1kOm+4*270kOm))/(1kOm*sqrt(2)) 	=  382,191 [V]

	���� ��� ����������� ���������� � ����
	IRMSref = round((In/Ifs)*9032007d) =   round((  5/ 110.485)* 9032007d) 	= 408743.585
	VRMSref = round((Vn/Vfs)*9032007d) =   round((230/ 382,191)* 9032007d) 	= 5435401.697

	������� ������ ���� �� 1 �����
	IRMS_SCALE  = IRMSref/In = 408743.585/5 		= 81748.717
	
	������� ������ ���� �� 1 �����
	VRMS_SCALE  = VRMSref/Vn = 5435401.697/230 		= 23632.181

	������� ������ ���� �� 1 ����
	POWER_SCALE = 1000*PMAX/(Vfs*Ifs) = (1000*4862401)/(382,191*110.485) = 115150.81

	��� �������� ����������� ����������� �������� 
	NOLOAD = round((Vn*Inoload*PMAX)/(Vfs*Ifs)) = round((230*0,015*4862401)/(382,191*110.485)) = 397.27


	����������� ���										5		 [�]
	����������� ����������  							230 	 [V]
	���, ��� ������� �� ����� ADE 0,5 ������			110.485  [�]
	����������, ��� ������� �� ����� ADE 0,5 ������		382.191  [V]
	���������� ��������	���� �� ����� 					16 

*/

/* Private variables ---------------------------------------------------------*/

/* Global variables ---------------------------------------------------------*/
extern bool stateADE_noload;
extern bool fwaitingForDisable;
extern __no_init struct 
{
	u32 val;
	u16 crc;
} timeOfWork;

extern struct{
	u8  ZX50;
	u8  StartZX20;  
} adeCnt;


/* Private function prototypes -----------------------------------------------*/

void ADE_SetResetStateClearVariables();
void processADE_ISR();
void processSecond();
void readoutADE();
void processSecond();
void Calculate_TMI_AEnergy_Plus();

/* Private functions ---------------------------------------------------------*/



/**
  * @brief  ����� ������ � ADE7953
  * @note   � ������ ���� ���������� �� ���� ADE
			���� ���� ��������� �������� ������
			���� ��������� ������� �� ������������, ������������ ����������
  * @retval None
  */
PT_THREAD(thread_ade())
{
  	static uint32_t tmp = 0;
	PT_STATE_INIT();
	PT_BEGIN(pt);
	
	
	disableInterrupts();
	
	ade.initPins();
	
	GPIO_Init(ADE_ISR_GPIO, ADE_ISR_PIN, GPIO_Mode_In_PU_IT);
	EXTI_SetPinSensitivity(ADE_EXTI_Pin_ISR, EXTI_Trigger_Falling);

	enableInterrupts();
	while(1) 
	{
	
		ade.resetSetSPI();

		// ** ��������� ������� ����� ������
		// ** ���� ���������� � ������������ ������ �����
		tim.reset(ADE_DEAD_TIMER);
		do 
		{
			if (msg.getVal(ADE_ISR_MSG))
			{
				tmp = ade.read(IRQSTATA_ADDR);

				if(IRQSTATA_RESET == (tmp & IRQSTATA_RESET))
				{
					ADE_SetResetStateClearVariables();
					break;
				}
			}
			PT_YIELD(pt);
		} while(tim.get(ADE_DEAD_TIMER) < 1000);

		
		
		// ** ����� ������ ���������
		if (tim.get(ADE_DEAD_TIMER) < 1000) {

			tim.reset(ADE_DEAD_TIMER);
			do {

				// ** ��������� ������� ����������
				if (msg.getVal(ADE_ISR_MSG))
				{
					tim.reset(ADE_DEAD_TIMER);
					
					
					tmp = ade.read(IRQSTATA_ADDR);	

					// ����������� ���� ZXV 
					if(IRQSTATA_ZXV == (tmp & IRQSTATA_ZXV)) 
					{
						fwaitingForDisable = false;
						
						
						// ** ���� ��������� ������� ����� ������ ADE
						if (adeCnt.StartZX20 < 20)
						{
							adeCnt.StartZX20++;	
						}
						// ������� ������ 
						else
						{
							readoutADE();
							Calculate_TMI_AEnergy_Plus();
							
							// ** ���������� 1 �������
							adeCnt.ZX50++;
							if(50 == adeCnt.ZX50)
							{
								adeCnt.ZX50 = 0;
		
								processSecond();
							}
						}

					} // ** ����� ��������� ����������� ����

					// ** ���������, ������ ����� ����������
					else if((tmp & IRQSTATA_ZXTO) == IRQSTATA_ZXTO) // ZXTO
					{
						// ** �������� ���������� � ����� ������
						saveTelemetry();						
						mem.writeDataTwoCopyes(I_EE_TIME_OF_WORK, &timeOfWork.val);
						
						fwaitingForDisable = true;
					}
					// ��������� ��������� "Reset" 
					else if(IRQSTATA_RESET == (tmp & IRQSTATA_RESET))
					{
            msg.set(ADE_RESET_MSG, NULL, 0);
					}
					
					
					
					// ** ��������� �� ���� ���������� �������� 
					tmp = ade.read(ADE_REG_ACCMODE);	
					if(0x00010000 != (tmp & 0x00010000)) {
						stateADE_noload = true;
					}
					else {
						stateADE_noload = false;
					}
				} // endif "ISR"
				
				// ** ������������ ADE
				if (msg.getVal(ADE_RESET_MSG))
				{
					ADE_RESET_GPIO->ODR &= ~ADE_RESET_PIN;
					delay_10us(2);
					ADE_RESET_GPIO->ODR |= ADE_RESET_PIN;
					
					break;
				}
				PT_YIELD(pt);

			} while(tim.get(ADE_DEAD_TIMER) < 1000);
		}
		
	}// endwhile(1)

	PT_END(pt);
}


/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/************************END OF FILE****/
