



#define DS18B20_SKIP_ROM            0xcc
#define DS18B20_CONVERT_T           0x44
#define DS18B20_READ_SCRATCHPAD     0xbe

#define WAIT_UNTIL_CONVERTION_COMPLETE while (0 == ds_readSlot())


#define   DS_port       GPIOC
#define   DS_pin        5
#define   DS_pin_MASK   (1 << DS_pin)

#define   _us /5 



#define   PORT_OUTPUT DS_port->DDR |=  DS_pin_MASK
#define   PORT_INPUT  DS_port->DDR &= ~DS_pin_MASK

#define   PIN_LOW	  DS_port->ODR &= ~DS_pin_MASK
#define   PIN_HIGH	  DS_port->ODR |=  DS_pin_MASK

#define   OUTPUT_HIGH() do {DS_port->DDR |=  DS_pin_MASK; DS_port->ODR |=   DS_pin_MASK;}while(0)
#define   OUTPUT_LOW()  do {DS_port->DDR |=  DS_pin_MASK; DS_port->ODR &=  ~DS_pin_MASK;}while(0)

// Delay in I * 5us
//void _delay_x5_us(u16 i);
//bool ds_reset();
//void ds_writeSlot(bool i);
//u8 ds_readSlot (void);
//void ds_writeByte (u8 byte);
//u8 ds_readByte (void);
//float convertTemperToFloat(u8* p);
//float get_temperature();