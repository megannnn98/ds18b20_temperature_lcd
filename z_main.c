/**
  ******************************************************************************
  * @file    z_main.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Function runs all threads
  ******************************************************************************  
  */


/* Includes ------------------------------------------------------------------*/
#include "HAL.h"
#include "lcd.h"
#include "delay.h"
#include "DS18B20.h"

/* Private function prototypes -----------------------------------------------*/
void supplyEn();
float get_temperature();
bool ds_reset();
/* Private functions ---------------------------------------------------------*/


/* Global variables ---------------------------------------------------------*/
volatile float temperature = 0.;
u8 cnt = 0;

/**
  * @brief  Main function
  * @param  None
  * @retval None
  */
void main( void )
{
    supplyEn();
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);

    void _delay_x5_us(u16 i);
    
    OUTPUT_HIGH();
    DS_port->CR1 |=  DS_pin_MASK;
    
    
    
    lcd.init();
    
    enableInterrupts();

    lcd.all();
    delay_ms(500);
    lcd.clear();
    delay_ms(200);
    
    OUTPUT_HIGH();
    if (TRUE == ds_reset()) {
        while(1);
    }
    
    lcd.show_lines();
    
    
    
    while(1)
    {
        temperature = get_temperature();
        lcd.show_float(temperature, FALSE, FALSE);
        lcd.text((bool)SHOW, 1, POINT);

        lcd.num_channel(cnt);
        if (cnt == 8) {
          cnt = 0;
        } else {
          cnt++;
        }
    }
}

/**
  * @}
  */

/************************END OF FILE****/